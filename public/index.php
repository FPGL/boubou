<?php

// Register The Auto Loader.
require_once __DIR__ . './../core/includes/autoload.php';

// Run the application.
require_once __DIR__ . './../core/includes/app.php';
