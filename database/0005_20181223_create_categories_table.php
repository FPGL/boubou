<?php

use Boubou\Core\Database\Migration;
use Boubou\Core\Database\Table;
use Boubou\Core\Database\Schema;

/**
 * Blog categories migration.
 */
class Categories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Table $table) {
            $table->primary('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::create('categories_posts', function (Table $table) {
            $table->primary('id');
            $table->foreign('category_id', 'categories', 'id')->onDelete('cascade');
            $table->foreign('post_id', 'posts', 'id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories_posts');
        Schema::drop('categories');
    }
}
