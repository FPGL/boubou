<?php

use Boubou\Core\Database\Migration;
use Boubou\Core\Database\Table;
use Boubou\Core\Database\Schema;

/**
 * User roles migration.
 */
class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Table $table) {
            $table->primary('id');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('roles_users', function (Table $table) {
            $table->primary('id');
            $table->foreign('role_id', 'roles', 'id')->onDelete('cascade');
            $table->foreign('user_id', 'users', 'id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles_users');
        Schema::drop('roles');
    }
}
