<?php

use Boubou\Core\Database\Migration;
use Boubou\Core\Database\Table;
use Boubou\Core\Database\Schema;

/**
 * Blog tags migration.
 */
class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Table $table) {
            $table->primary('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::create('tags_posts', function (Table $table) {
            $table->primary('id');
            $table->foreign('tag_id', 'tags', 'id')->onDelete('cascade');
            $table->foreign('post_id', 'posts', 'id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tags_posts');
        Schema::drop('tags');
    }
}
