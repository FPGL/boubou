<?php

use Boubou\Core\Database\Migration;
use Boubou\Core\Database\Table;
use Boubou\Core\Database\Schema;

/**
 * User infos migration.
 */
class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Table $table) {
            $table->primary('id');
            $table->foreign('user_id', 'users', 'id')->onDelete('cascade');
            $table->text('firstname');
            $table->string('lastname');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_infos');
    }
}
