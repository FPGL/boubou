<?php

use Boubou\Core\Database\Migration;
use Boubou\Core\Database\Table;
use Boubou\Core\Database\Schema;

/**
 * User migration.
 */
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Table $table) {
            $table->primary('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            // $table->string('description');
            // $table->text('description');
            // $table->integer('age');
            // $table->decimal('price', 19, 2)->default(0);
            // $table->boolean('active')->default(true);
            // $table->enum('gender', ['male', 'female', 'other'])->nullable()->default('customer');
            // $table->foreign('parent_id', 'users', 'id')->onDelete('cascade');
            // $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
