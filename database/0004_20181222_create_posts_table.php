<?php

use Boubou\Core\Database\Migration;
use Boubou\Core\Database\Table;
use Boubou\Core\Database\Schema;

/**
 * Blog posts migration.
 */
class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Table $table) {
            $table->primary('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('picture')->nullable();
            $table->text('resume')->nullable();
            $table->text('article');
            $table->date('published_at')->nullable();
            $table->foreign('user_id', 'users', 'id')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
