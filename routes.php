<?php

use Boubou\Core\Route;

/*
Your web routes!
*/

Route::prefix('/admin', function () {

    // Admin dashboard
    Route::get('/', 'UsersController@dashboard');

    // Users
    Route::get('/users', 'UsersController@index');
    Route::post('/users', 'UsersController@store');
    Route::get('/users/create', 'UsersController@create');
    Route::get('/users/{id}', 'UsersController@show');
    Route::patch('/users/{id}', 'UsersController@update');
    Route::get('/users/{id}/edit', 'UsersController@edit');
    Route::get('/users/{id}/destroy', 'UsersController@destroy');

    // Posts
    Route::prefix('/posts', function () {
        Route::get('/', 'PostsController@index');
        Route::post('/', 'PostsController@store');
        Route::get('/create', 'PostsController@create');
        Route::get('/{id}', 'PostsController@show');
        Route::patch('/{id}', 'PostsController@update');
        Route::get('/{id}/edit', 'PostsController@edit');
        Route::get('/{id}/destroy', 'PostsController@destroy');
    });

    // Categories
    Route::prefix('/categories', function () {
        Route::get('/', 'CategoriesController@index');
        Route::post('/', 'CategoriesController@store');
        Route::get('/create', 'CategoriesController@create');
        Route::get('/{id}', 'CategoriesController@show');
        Route::patch('/{id}', 'CategoriesController@update');
        Route::get('/{id}/edit', 'CategoriesController@edit');
        Route::get('/{id}/destroy', 'CategoriesController@destroy');
    });

    // Tags
    Route::prefix('/tags', function () {
        Route::get('/', 'TagsController@index');
        Route::post('/', 'TagsController@store');
        Route::get('/create', 'TagsController@create');
        Route::get('/{id}', 'TagsController@show');
        Route::patch('/{id}', 'TagsController@update');
        Route::get('/{id}/edit', 'TagsController@edit');
        Route::get('/{id}/destroy', 'TagsController@destroy');
    });
});

// Blog
Route::prefix('/posts', function () {
    Route::get('/', 'BlogController@index');
    Route::get('/{slug}', 'BlogController@show');
});
Route::get('/users/{id}', 'BlogController@user');
Route::get('/categories/{slug}', 'BlogController@category');
Route::get('/tags/{slug}', 'BlogController@tag');

// Authentication
Route::get('/login', 'AuthController@login');
Route::post('/login', 'AuthController@attemptLogin');
Route::get('/register', 'AuthController@register');
Route::post('/register', 'AuthController@store');
Route::get('/logout', 'AuthController@logout');

Route::get('/home', 'UsersController@dashboard');

Route::get('/', 'WelcomeController@welcome');
