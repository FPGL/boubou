/**
 * String to slug.
 *
 * @param {string} s
 *
 * @return string
 */
function slug(s) {
  let slug = s.replace(/[^-\d\a-zA-Z]/g, '-')
    .replace(/[-]+/g, '-')
    .replace(/^-/g, '')
    .replace(/-$/g, '');

  return slug.toLowerCase();
}
