<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>{{ env('APP_NAME', 'My App') }}</title>
        <meta name="description" content="{{ env('APP_NAME', 'My App') }}">
        <meta name="author" content="{{ env('APP_NAME', 'My App') }}">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/app.css?v=1.0">

        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,600,700" rel="stylesheet">

    </head>

    <body>
        <div id="wrapper">
            <div class="row">

                <div id="sidebar-wrapper" class="col-md-2">
                    <div class="sidebar-brand">
                        <a href="/">
                            <img src="/images/boubou-logo.svg" alt="Boubou" width="72" height="72" />
                        </a>
                        <p>&lt;?php Boubou ?&gt;</p>
                    </div>
                    @include('admin.partials.nav')
                </div>

                <div id="page-content-wrapper" class="col-md-10">
                    <section class="container-fluid text-right">
                        @include('partials.auth-btns')
                    </section>

                    <section class="container-fluid mb-5">
                        <h1 class="h2"><b>Boubou</b><br />
                            <small style="font-size: 60%;">A small PHP framework</small></h1>
                    </section>

                    <div class="row mt-5">
                        <div class="col-xl-8">
                            <section class="container-fluid">
                                @yield('content')
                            </section>
                        </div>
                    </div>

                    @include('partials.footer')

                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="/js/app.js"></script>
        @include('partials.analytics')

    </body>

</html>
