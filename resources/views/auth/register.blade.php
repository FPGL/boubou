<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Register</title>

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/login.css" rel="stylesheet">
    </head>

    <body class="text-center">
        <div class="form-signin">
            <form method="post">
                <input type="hidden" name="csrf_token" value="{{ Csrf::token() }}" />
                <img src="/images/boubou-logo.svg" alt="Boubou" width="72" height="72" />
                <h1 class="h3 mt-4 mb-3 font-weight-normal">Please register</h1>

                    <label for="name" class="sr-only">Name</label>
                    <input type="text" id="name" name="name" class="form-control form-control-first" placeholder="Name" required autofocus />

                    <label for="email" class="sr-only">Email address</label>
                    <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required />

                    <label for="password" class="sr-only">Password</label>
                    <input type="password" id="password" name="password" class="form-control form-control-last" placeholder="Password" required />

                    {{-- <div class="checkbox mb-3">
                        <label>
                            <input type="checkbox" value="remember-me" /> Remember me
                        </label>
                    </div> --}}

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>

            <p class="mt-5"><a href="/login">Already registered? Log in!</a></p>
            <p class="mt-5 mb-3 pt-3 text-muted">&copy; 2018-{{ date('Y') }}</p>
        </div>

    </body>

</html>
