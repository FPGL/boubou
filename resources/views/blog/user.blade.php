@extends('layouts.app')

@section('content')

    <h2 class="h3 mb-4">By {{ $user->name }}</h2>

    <?php $posts = $user->posts()->paginate() ?>

    @if (! $posts->total())
        <p class="lead">No post!</p>
    @endif

    @foreach ($posts as $k => $post)
        @include('partials.post-resume', ['post' => $post])
    @endforeach

    @include('partials.pagination', ['list' => $posts, 'q' => request()->get('q')])

@endsection
