@extends('layouts.app')

@section('content')

    @if (! $posts->total())
        <p class="lead">No post!</p>
    @endif

    @foreach ($posts as $k => $post)
        @include('partials.post-resume', ['post' => $post])
    @endforeach

    @include('partials.pagination', ['list' => $posts, 'q' => request()->get('q')])

@endsection
