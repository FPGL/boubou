@extends('layouts.app')

@section('content')

    <h2 class="h3 mb-4">{{ $category->name }}</h2>

    <?php $posts = $category->posts()->paginate() ?>

    @if (! $posts->total())
        <p class="lead">No post!</p>
    @endif

    @foreach ($posts as $post)
        @include('partials.post-resume', ['post' => $post])
    @endforeach

    @include('partials.pagination', ['list' => $posts, 'q' => request()->get('q')])

@endsection
