@extends('layouts.app')

@section('content')
    <article class="post">
        <div class="post-header">
            <h2 class="h3"><span>{{ $post->title }}</span></h2>
            <ul class="post-meta">
                <li><i class="far fa-calendar fa-fw"></i> {{ date('M d, Y', strtotime($post->created_at)) }}</li>
                @if (count($post->categories))
                    <li><i class="fas fa-tags fa-fw"></i>
                        @foreach ($post->categories as $category)
                            <a href="/categories/{{ $category->slug }}">{{ $category->name }}</a>,
                        @endforeach</li>
                @endif
                @if (count($post->tags))
                    <li><i class="fas fa-tags fa-fw"></i>
                        @foreach ($post->tags as $tag)
                            <a href="/tags/{{ $tag->slug }}">{{ $tag->name }}</a>,
                        @endforeach</li>
                @endif
                <li><i class="fas fa-user fa-fw"></i> By <a href="/users/{{ $post->user->id }}"><b>{{ $post->user->name }}</b></a></li>
            </ul>
        </div>
        @if ($post->picture)
        <div class="post-preview">
            {{-- <img src="/images/blog-1.jpg" alt="" class="img-fluid rounded"> --}}
            <img src="/uploads/{{ $post->picture }}" alt="" class="img-fluid rounded" />
        </div>
        @endif
        <div class="post-content">
            <div class="resume lead mb-3">{{ $post->resume }}</div>
            <div class="article">{!! nl2br($post->article) !!}</div>
        </div>
    </article>
@endsection
