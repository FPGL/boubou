<div class="sidebar">
    <aside>
        @include('partials.search', ['action' => '/posts'])
    </aside>
    <aside>
        <div class="h3">About</div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam non sollicitudin metus.</p>
    </aside>
    <aside class="social">
        <div class="h3">Subscribe & Follow</div>
        <a href="#"><i class="fab fa-facebook-f"></i></a>
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-pinterest-p"></i></a>
    </aside>
    @if (isset($categories) && count($categories))
    <aside>
        <div class="h3">Categories</div>
        <ul class="list-unstyled">
            @foreach ($categories as $k => $category)
            <li><a href="/categories/{{ $category->slug }}">{{ $category->name }} ({{ $category->posts->total() }})</a></li>
            @endforeach
        </ul>
    </aside>
    @endif
    {{-- <aside>
        <div class="h3">Archives</div>
        <ul>
            <li>March 2017 (40)</li>
        </ul>
    </aside> --}}
    @if (isset($tags) && count($tags))
    <aside>
        <div class="h3">Tags</div>
        @foreach ($tags as $k => $tag)
        <a href="/tags/{{ $tag->slug }}" class="badge badge-light">{{ $tag->name }} ({{ $tag->posts->total() }})</a>
        @endforeach
        {{-- <a href="#" class="badge badge-primary">Primary</a>
        <a href="#" class="badge badge-secondary">Secondary</a>
        <a href="#" class="badge badge-success">Success</a>
        <a href="#" class="badge badge-danger">Danger</a>
        <a href="#" class="badge badge-warning">Warning</a>
        <a href="#" class="badge badge-info">Info</a>
        <a href="#" class="badge badge-light">Light</a>
        <a href="#" class="badge badge-dark">Dark</a> --}}
    </aside>
    @endif
</div>
