<ul class="sidebar-nav">
    <li>
        <a class="{{ request()->url() == '/' ? 'active' : '' }}" href="/">Home</a>
    </li>
    <li>
        <a class="{{ request()->match('/posts*') || request()->match('/categories*') ? 'active' : '' }}" href="/posts">Blog</a>
    </li>
    <li>
        <a href="/dox/html">Documentation</a>
    </li>
</ul>
