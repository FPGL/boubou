<ul class="auth-btns">
@if (Auth::user())
    <li><span>{{ Auth::user()->name }}</span> <u>{{ Auth::user()->email }}</u></li>
    <li><a href="/home"><i class="fas fa-tachometer-alt fa-fw"></i> Admin</a></li>
    <li><a href="/logout"><i class="fas fa-sign-out-alt fa-fw"></i> Logout</a></li>
@else
    <li><a href="/login"><i class="fas fa-sign-in-alt fa-fw"></i> Login</a><li>
    <li><a href="/register"><i class="fas fa-user fa-fw"></i> Register</a></li>
@endif
</ul>
