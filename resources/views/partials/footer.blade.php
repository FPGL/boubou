<footer class="footer m-3">
    <span class="text-muted">Copyright (c) {{ date('Y') }} <a href="{!! obfuscate('mailto:fpglaurent@gmail.com') !!}">FPGLaurent</a> - <a href="https://gitlab.com/FPGL/boubou" target="_blank">https://gitlab.com/FPGL/boubou</a></span>
</footer>
