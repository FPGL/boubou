<article class="post">
    <div class="post-header">
        <h2 class="post-title"><a href="/posts/{{ $post->slug }}">{{ $post->title }}</a></h2>
        <ul class="post-meta">
            <li><i class="far fa-calendar fa-fw"></i> {{ date('M d, Y', strtotime($post->created_at)) }}</li>
            @if (count($post->categories))
            <li><i class="fas fa-tags fa-fw"></i>
                @foreach ($post->categories as $category)
                    <a href="/categories/{{ $category->slug }}">{{ $category->name }}</a>,
                @endforeach</li>
            @endif
            <li><i class="fas fa-user fa-fw"></i> By <a href="/users/{{ $post->user->id }}"><b>{{ $post->user->name }}</b></a></li>
        </ul>
    </div>
    @if ($post->picture)
        <div class="post-preview">
            {{-- <img src="/images/blog-1.jpg" alt="" class="img-fluid rounded"> --}}
            <img src="/uploads/{{ $post->picture }}" alt="" class="img-fluid rounded" />
        </div>
    @endif
    <div class="post-content">
        <p>{{ $post->resume }}</p>
    </div>
    <p><a href="/posts/{{ $post->slug }}" class="btn btn-outline-primary btn-sm">Read More <i class="fas fa-arrow-right fa-fw fa-xs"></i></a></p>
</article>
