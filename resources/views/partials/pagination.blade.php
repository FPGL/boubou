@if ($list->numberOfPages() > 1)
<nav aria-label="Pagination">
    <ul class="pagination">
        <li class="page-item {{ $list->currentPage() == 1 ? 'disabled' : '' }}">
            <a class="page-link" href="?page={{ $list->previousPage() }}{{ ! empty($q) ? '&q=' . urlencode($q) : '' }}" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a></li>

        @for ($i = $list->startPage(); $i <= $list->maxPage(); $i++)
            @if ($i <= $list->numberOfPages())
                <li class="page-item {{ $list->currentPage() == $i ? 'active' : '' }}">
                    <a class="page-link" href="?page={{ $i }}{{ ! empty($q) ? '&q=' . urlencode($q) : '' }}">{{ $i }}</a>
                </li>
            @else
                <li class="page-item disabled"><a class="page-link">{{ $i }}</a></li>
            @endif
        @endfor

        <li class="page-item {{ ($list->currentPage() == $list->numberOfPages() ? 'disabled' : '') }}">
            <a class="page-link" href="?page={{ $list->nextPage() }}{{ ! empty($q) ? '&q=' . urlencode($q) : '' }}" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a></li>
    </ul>
</nav>
@endif
