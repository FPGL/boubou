<form action="{{ $action }}" method="get">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search for..." value="{{ request()->get('q') }}" />
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search fa-fw"></i></button>
        </div>
    </div>
</form>
