<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>403 - Forbidden</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/app.css?v=1.0">

        <style>
        html, body{
            height:100%;
            margin:0;
            padding:0;
            background-color: #f5f5f5;
        }
        .container-fluid
        {
          height:100%;
          display:table;
          width: 100%;
          padding: 0;
        }
        .row-fluid {
            height: 100%;
            display:table-cell;
            vertical-align: middle;
        }
        .centering
        {
            float:none;
            margin:0 auto;
        }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="centering text-center">
                    <img src="/images/boubou-logo.svg" alt="Boubou" width="72" height="72" />
                    <h2 class="h3">[{{ $code }}] Forbidden</h2>
                    @if (! empty($message))
                    <p>{{ $message }}</p>
                    @endif
                </div>
            </div>
        </div>
        @include('partials.analytics')
    </body>
</html>
