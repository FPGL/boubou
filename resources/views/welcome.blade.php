@extends('layouts.app')

@section('content')

{{-- <h2 class="h2">Boubou</h2> --}}
<p>This is a PHP blog demo, designed entirely with Boubou.<br />
    The complete sources are on gitlab: <a href="https://gitlab.com/FPGL/boubou" target="_blank">https://gitlab.com/FPGL/boubou</a>.</p>

<p><a href="/posts" class="btn btn-outline-primary">Access the blog <i class="fas fa-arrow-right fa-fw fa-xs"></i></a></p>

@endsection
