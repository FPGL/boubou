@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6">
        @include('partials.search', ['action' => '/admin/posts'])
    </div>
    <div class="col-md-6">
        <p class="text-right"><a href="/admin/posts/create" class="btn btn-primary">Create post</a></p>
    </div>
</div>

@include('admin.partials.posts', ['posts' => $posts])

@endsection
