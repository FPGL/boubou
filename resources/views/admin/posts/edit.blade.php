@extends('layouts.admin')

@section('content')

<h2 class="h3">Edit a post</h2>
<form method="post" action="/admin/posts/{{ $post->id }}" enctype="multipart/form-data">
    <input type="hidden" name="csrf_token" value="{{ Csrf::token() }}" />
    <input type="hidden" name="_method" value="patch" />
    <input type="hidden" name="id" value="{{ $post->id }}" />
    <div>
        <label>Title</label><br />
        <input type="text" name="title" value="{{ $post->title }}" class="form-control" /><br />
        <label>Slug</label><br />
        <div class="input-group">
            <div class="input-group-prepend">
                <button class="btn btn-outline-secondary" type="button" onclick="$('#slug').val(window.slug($('#slug').val()));"><i class="fas fa-link fa-fw"></i></button>
            </div>
            <input type="text" name="slug" id="slug" value="{{ $post->slug }}" class="form-control" />
        </div>
        <br />
        @if ($post->picture)
            <div class="mb-2"><img src="/uploads/{{ $post->picture }}" alt="" class="img-fluid rounded" /></div>
        @endif
        <label>Picture</label><br />
        <input type="file" name="file" class="form-control-file" /><br />
        <label>Resume</label><br />
        <textarea type="text" name="resume" class="form-control">{{ $post->resume }}</textarea><br />
        <label>Article</label><br />
        <textarea type="text" name="article" class="form-control">{{ $post->article }}</textarea><br />
        <label>Category</label><br />
        <select name="category_ids[]" class="custom-select">
            <option value="">None</option>
            @foreach ($categories as $category)
            <option value="{{ $category->id }}" {{ $post->categories()->has($category->id) ? 'selected' : '' }}>{{ $category->name }}</option>
            @endforeach
        </select>
        <label>Tags</label><br />
        <select name="tag_ids[]" class="custom-select" multiple>
            <option value="">None</option>
            @foreach ($tags as $tag)
            <option value="{{ $tag->id }}" {{ $post->categories()->has($tag->id) ? 'selected' : '' }}>{{ $tag->name }}</option>
            @endforeach
        </select>
    </div>
    <hr />
    <div>
        <button type="submit" class="btn btn-primary btn-sm">Store</button>
        <a href="/admin/posts" class="btn btn-link">Cancel</a>
    </div>
</form>

@endsection
