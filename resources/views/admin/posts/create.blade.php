@extends('layouts.admin')

@section('content')

<h2 class="h3">Create a post</h2>
<form method="post" action="/admin/posts" enctype="multipart/form-data">
    <input type="hidden" name="csrf_token" value="{{ Csrf::token() }}" />
    <div>
        <label>Title</label><br />
        <input type="text" name="title" value="" class="form-control" /><br />
        <label>Slug</label><br />
        <div class="input-group">
            <div class="input-group-prepend">
                <button class="btn btn-outline-secondary" type="button" onclick="$('#slug').val(window.slug($('#slug').val()));"><i class="fas fa-link fa-fw"></i></button>
            </div>
            <input type="text" name="slug" id="slug" value="" class="form-control" />
        </div>
        <br />
        <label>Picture</label><br />
        <input type="file" name="file" class="form-control-file" /><br />
        <label>Resume</label><br />
        <textarea type="text" name="resume" class="form-control"></textarea><br />
        <label>Article</label><br />
        <textarea type="text" name="article" class="form-control"></textarea><br />
        <label>Category</label><br />
        <select name="category_ids[]" class="custom-select">
            <option value="">None</option>
            @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
        <label>Tags</label><br />
        <select name="tag_ids[]" class="custom-select" multiple>
            <option value="">None</option>
            @foreach ($tags as $tag)
                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
            @endforeach
        </select>
    </div>
    <hr />
    <div>
        <button type="submit" class="btn btn-primary">Create</button>
        <a href="/admin/posts" class="btn btn-link">Cancel</a>
    </div>
</form>

@endsection
