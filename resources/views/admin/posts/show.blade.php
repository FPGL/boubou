@extends('layouts.admin')

@section('content')

<p class="float-right">
    <a href="/admin/posts/{{ $post->id }}/edit" class="btn btn-primary btn-sm">Edit</a>
    <a href="/admin/posts/{{ $post->id }}/destroy" class="btn btn-danger btn-sm">Destroy</a>
</p>

<code>ID#{{ $post->id }}</code><br />

<article class="post">
    <div class="post-header">
        <h2 class="h3"><span>{{ $post->title }}</span></h2>
        <ul class="post-meta">
            <li><i class="far fa-calendar fa-fw"></i> {{ date('M d, Y', strtotime($post->created_at)) }}</li>
            @if (count($post->categories))
                <li><i class="fas fa-tags fa-fw"></i>
                    {{-- @foreach ($post->categories()->get() as $category) --}}
                    @foreach ($post->categories as $category)
                        <a href="/admin/categories/{{ $category->id }}">{{ $category->name }}</a>,
                    @endforeach</li>
            @endif
            @if (count($post->tags))
                <li><i class="fas fa-tags fa-fw"></i>
                    {{-- @foreach ($post->tags()->get() as $tag) --}}
                    @foreach ($post->tags as $tag)
                        <a href="/admin/tags/{{ $tag->id }}">{{ $tag->name }}</a>,
                    @endforeach</li>
            @endif
            <li><i class="fas fa-user fa-fw"></i> By <a href="/admin/users/{{ $post->user->id }}"><b>{{ $post->user->name }}</b></a></li>
        </ul>
    </div>
    @if ($post->picture)
        <div class="post-preview">
            <img src="/uploads/{{ $post->picture }}" alt="" class="img-fluid rounded" />
        </div>
    @endif
    <div class="post-content">
        <div class="resume">{{ $post->resume }}</div>
        <div class="article">{!! nl2br($post->article) !!}</div>
    </div>
</article>

<hr />
<p>
    <a href="/admin/posts" class="btn btn-link">Back</a>
</p>

@endsection
