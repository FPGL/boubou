<ul class="sidebar-nav">
    <li>
        <a class="{{ request()->url() == '/admin' || request()->url() == '/home' ? 'active' : '' }}" href="/admin">Dashboard <span class="sr-only">(current)</span></a>
    </li>
    <li>
        <a class="{{ request()->match('/admin/posts*') ? 'active' : '' }}" href="/admin/posts">Posts</a>
    </li>
    <li>
        <a class="{{ request()->match('/admin/categories*') ? 'active' : '' }}" href="/admin/categories">Categories</a>
    </li>
    <li>
        <a class="{{ request()->match('/admin/tags*') ? 'active' : '' }}" href="/admin/tags">Tags</a>
    </li>
    <li>
        <a class="{{ request()->match('/admin/users*') ? 'active' : '' }}" href="/admin/users">Users</a>
    </li>
</ul>
