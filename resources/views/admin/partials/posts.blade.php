<br />
@if (! $posts->total())
    <p class="lead">No post!</p>
@else

    <p>Total categories: {{ $posts->total() }}</p>

    <table class="table">
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">#</th>
                <th scope="col">Post</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts  as $k => $post)
            <tr>
                <td class="nobr">
                    <a href="/admin/posts/{{ $post->id }}/edit" class="btn btn-outline-primary btn-sm">Edit</a>
                    <a href="/admin/posts/{{ $post->id }}/destroy" class="btn btn-outline-danger btn-sm">Destroy</a>
                </td>
                <td class="nobr">
                    <code>{{ $post->id }}</code>
                </td>
                <td class="w-100">
                    @if ($post->picture)
                        <div class="mb-2"><img src="/uploads/{{ $post->picture }}" alt="" class="img-fluid rounded" /></div>
                    @endif
                    <a href="/admin/posts/{{ $post->id }}">{{ $post->title }}</a>
                    <div class="mb-2">
                        @if (count($post->categories))
                        <small>Category:
                            @foreach ($post->categories()->get() as $category)
                            <a href="/admin/categories/{{ $category->id }}">{{ $category->name }}</a>
                            @endforeach
                        </small><br />
                        @endif
                        @if (count($post->tags))
                        <small>Tags:
                            {{-- @foreach ($post->tags()->get() as $tag) --}}
                            @foreach ($post->tags as $tag)
                                <a href="/admin/tags/{{ $tag->id }}">{{ $tag->name }}</a>
                            @endforeach
                        </small><br />
                        @endif
                        <small>Posted by <a href="/admin/users/{{ $post->user->id }}"><b>{{ $post->user->name }}</b></a></small><br />
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    @include('partials.pagination', ['list' => $posts, 'q' => request()->get('q')])

@endif
