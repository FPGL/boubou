@extends('layouts.admin')

@section('content')

<h2 class="h3">Create a category</h2>
<form method="post" action="/admin/categories">
    <input type="hidden" name="csrf_token" value="{{ Csrf::token() }}" />
    <div>
        <label>Name</label><br />
        <input type="text" name="name" value="" class="form-control" /><br />
        <label>Slug</label><br />
        <div class="input-group">
            <div class="input-group-prepend">
                <button class="btn btn-outline-secondary" type="button" onclick="$('#slug').val(window.slug($('#slug').val()));"><i class="fas fa-link fa-fw"></i></button>
            </div>
            <input type="text" name="slug" id="slug" value="" class="form-control" />
        </div>
        <br />
    </div>
    <hr />
    <div>
        <button type="submit" class="btn btn-primary">Create</button>
        <a href="/admin/categories" class="btn btn-link">Cancel</a>
    </div>
</form>

@endsection
