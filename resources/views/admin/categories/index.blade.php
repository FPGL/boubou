@extends('layouts.admin')

@section('content')

{{-- <h2 class="h3">Categories list</h2> --}}
<p class="text-right"><a href="/admin/categories/create" class="btn btn-primary">Create category</a></p>

{{-- {{ dump($categories) }} --}}

@if (! $categories->total())
    <p class="lead">No category!</p>
@endif
<p>Total categories: {{ $categories->total() }}</p>
<table class="table">
    <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">#</th>
            <th scope="col">Category</th>
            <th scope="col" class="nobr">Nb posts</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($categories as $k => $category)
            <tr>
                <td class="nobr">
                    <a href="/admin/categories/{{ $category->id }}/edit" class="btn btn-outline-primary btn-sm">Edit</a>
                    <a href="/admin/categories/{{ $category->id }}/destroy" class="btn btn-outline-danger btn-sm">Destroy</a>
                </td>
                <td class="nobr">
                    <code>{{ $category->id }}</code><br />
                </td>
                <td class="w-100">
                    <a href="/admin/categories/{{ $category->id }}">{{ $category->name }}</a>
                </td>
                <td class="text-right">
                    <span class="badge badge-{{ $category->posts()->length() ? 'info' : 'warning' }}">{{ $category->posts()->length() }}</span><br />
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@include('partials.pagination', ['list' => $categories, 'q' => ''])

@endsection
