@extends('layouts.admin')

@section('content')

<p class="float-right">
    <a href="/admin/categories/{{ $category->id }}/edit" class="btn btn-primary btn-sm">Edit</a>
    <a href="/admin/categories/{{ $category->id }}/destroy" class="btn btn-danger btn-sm">Destroy</a>
</p>

<div>
    <code>ID#{{ $category->id }}</code><br />
    <b>{{ $category->name }}</b><br />
    <p>{{ $category->description ?? 'No description.' }}</p>
</div>

@include('admin.partials.posts', ['posts' => $category->posts()->paginate()])

<hr />

<p>
    <a href="/admin/categories" class="btn btn-link">Back</a>
</p>

@endsection
