@extends('layouts.admin')

@section('content')

<h2 class="h3">Edit a category</h2>
<form method="post" action="/admin/categories/{{ $category->id }}">
    <input type="hidden" name="csrf_token" value="{{ Csrf::token() }}" />
    <input type="hidden" name="_method" value="patch" />
    <input type="hidden" name="id" value="{{ $category->id }}" />
    <div>
        <label>Name</label><br />
        <input type="text" name="name" value="{{ $category->name }}" class="form-control" /><br />
        <div class="input-group">
            <div class="input-group-prepend">
                <button class="btn btn-outline-secondary" type="button" onclick="$('#slug').val(window.slug($('#slug').val()));"><i class="fas fa-link fa-fw"></i></button>
            </div>
            <input type="text" name="slug" id="slug" value="{{ $category->slug }}" class="form-control" />
        </div>
        <br />
    </div>
    <hr />
    <div>
        <button type="submit" class="btn btn-primary btn-sm">Store</button>
        <a href="/admin/categories" class="btn btn-link">Cancel</a>
    </div>
</form>

@endsection
