@extends('layouts.admin')

@section('content')

<h2 class="h3">Edit an user</h2>
<form method="post" action="/admin/users/{{ $user->id }}">
    <input type="hidden" name="csrf_token" value="{{ Csrf::token() }}" />
    <input type="hidden" name="_method" value="patch" />
    <input type="hidden" name="id" value="{{ $user->id }}" />
    <div>
        <label>Name</label><br />
        <input type="text" name="name" value="{{ $user->name }}" class="form-control" /><br />
        <label>Email</label><br />
        <input type="text" name="email" value="{{ $user->email }}" class="form-control" /><br />
        <label>Password</label><br />
        <input type="password" name="password" value="" class="form-control" /><br />
        <label>Role</label><br />
        <select name="role_ids[]" class="custom-select" multiple>
            <option>None</option>
            @foreach ($roles as $role)
                <option value="{{ $role->id }}" {{ $user->roles()->has($role->id) ? 'selected' : '' }}>{{ $role->name }}</option>
            @endforeach
        </select>
        <hr />
        <label>Firstname</label><br />
        <input type="text" name="firstname" value="{{ $user->infos ? $user->infos->firstname : '' }}" class="form-control" /><br />
        <label>Lastname</label><br />
        <input type="text" name="lastname" value="{{ $user->infos ? $user->infos->lastname : '' }}" class="form-control" />
    </div>
    <hr />
    <div>
        <button type="submit" class="btn btn-primary btn-sm">Store</button>
        <a href="/admin/users" class="btn btn-link">Cancel</a>
    </div>
</form>

@endsection
