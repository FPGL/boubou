@extends('layouts.admin')

@section('content')

<h2 class="h3">Create an user</h2>
<form method="post" action="/admin/users">
    <input type="hidden" name="csrf_token" value="{{ Csrf::token() }}" />
    <div>
        <label>Name</label><br />
        <input type="text" name="name" value="" class="form-control" /><br />
        <label>Email</label><br />
        <input type="text" name="email" value="" class="form-control" /><br />
        <label>Password</label><br />
        <input type="password" name="password" value="" class="form-control" /><br />
        <label>Role</label><br />
        <select name="role_ids[]" class="custom-select" multiple>
            <option value="">None</option>
            @foreach ($roles as $role)
                <option value="{{ $role->id }}">{{ $role->name }}</option>
            @endforeach
        </select>

    </div>
    <hr />
    <div>
        <button type="submit" class="btn btn-primary">Create</button>
        <a href="/admin/users" class="btn btn-link">Cancel</a>
    </div>
</form>

@endsection
