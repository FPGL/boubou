@extends('layouts.admin')

@section('content')

<p class="float-right">
    <a href="/admin/users/{{ $user->id }}/edit" class="btn btn-primary btn-sm">Edit</a>
    <a href="/admin/users/{{ $user->id }}/destroy" class="btn btn-danger btn-sm">Destroy</a>
</p>

<p>
    <code>ID#{{ $user->id }}</code><br />
    <b>{{ $user->name }}</b><br />
    <u>{{ $user->email }}</u><br />
    Admin: {{ $user->hasRole('Admin') ? 'yes' : 'no' }}<br />
    Roles: {{ json($user->roles()->list('name')) }}<br />
</p>

@if ($user->infos->id)
    <h3>Infos:</h3>
    <p>
        Firstname: {{ $user->infos->firstname }}<br />
        Lastname: {{ $user->infos->lastname }}<br />
    </p>
@endif

@include('admin.partials.posts', ['posts' => $user->posts()->paginate()])

<hr />
<p><a href="/admin/users" class="btn btn-link">Back</a></p>

@endsection
