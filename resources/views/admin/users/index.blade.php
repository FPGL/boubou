@extends('layouts.admin')

@section('content')

{{-- <h2 class="h3">Users list</h2> --}}
<p class="text-right"><a href="/admin/users/create" class="btn btn-primary">Create user</a></p>

@if (! $users->total())
<p class="lead">No user!</p>
@endif

<p>Total users: {{ $users->total() }}</p>

<table class="table">
    <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">#</th>
            <th scope="col">User</th>
            <th scope="col" class="nobr">Nb posts</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $k => $user)
        <tr>
            <td class="nobr">
                <a href="/admin/users/{{ $user->id }}/edit" class="btn btn-outline-primary btn-sm">Edit</a>
                <a href="/admin/users/{{ $user->id }}/destroy" class="btn btn-outline-danger btn-sm">Destroy</a>
            </td>
            <td class="nobr">
                <code>{{ $user->id }}</code><br />
            </td>
            <td class="w-100">
                <a href="/admin/users/{{ $user->id }}">{{ $user->name }}</a><br />
                <b>{{ $user->email }}</b><br />
                {{-- admin: {{ $user->hasRole('Admin') ? 'yes' : 'no' }}<br /> --}}
                roles: {{ json($user->roles()->list('name')) }}<br />
            </td>
            <td class="text-right">
                <span class="badge badge-{{ $user->posts()->length() ? 'info' : 'warning' }}">{{ $user->posts()->length() }}</span><br />
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@include('partials.pagination', ['list' => $users])

@endsection
