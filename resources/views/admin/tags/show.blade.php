@extends('layouts.admin')

@section('content')

<p class="float-right">
    <a href="/admin/tags/{{ $tag->id }}/edit" class="btn btn-primary btn-sm">Edit</a>
    <a href="/admin/tags/{{ $tag->id }}/destroy" class="btn btn-danger btn-sm">Destroy</a>
</p>

<div>
    <code>ID#{{ $tag->id }}</code><br />
    <b>{{ $tag->name }}</b><br />
    <p>{{ $tag->description ?? 'No description.' }}</p>
</div>

@include('admin.partials.posts', ['posts' => $tag->posts()->paginate()])

<hr />

<p>
    <a href="/admin/tags" class="btn btn-link">Back</a>
</p>

@endsection
