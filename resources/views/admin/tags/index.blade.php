@extends('layouts.admin')

@section('content')

{{-- <h2 class="h3">Categories list</h2> --}}
<p class="text-right"><a href="/admin/tags/create" class="btn btn-primary">Create tag</a></p>

{{-- {{ dump($tags) }} --}}

@if (! $tags->total())
    <p class="lead">No tag!</p>
@endif
<p>Total tags: {{ $tags->total() }}</p>
<table class="table">
    <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">#</th>
            <th scope="col">Tag</th>
            <th scope="col" class="nobr">Nb posts</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tags as $k => $tag)
            <tr>
                <td class="nobr">
                    <a href="/admin/tags/{{ $tag->id }}/edit" class="btn btn-outline-primary btn-sm">Edit</a>
                    <a href="/admin/tags/{{ $tag->id }}/destroy" class="btn btn-outline-danger btn-sm">Destroy</a>
                </td>
                <td class="nobr">
                    <code>{{ $tag->id }}</code><br />
                </td>
                <td class="w-100">
                    <a href="/admin/tags/{{ $tag->id }}">{{ $tag->name }}</a>
                </td>
                <td class="text-right">
                    <span class="badge badge-{{ $tag->posts()->length() ? 'info' : 'warning' }}">{{ $tag->posts()->length() }}</span><br />
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@include('partials.pagination', ['list' => $tags, 'q' => ''])

@endsection
