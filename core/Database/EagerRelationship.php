<?php

namespace Boubou\Core\Database;

/**
 * Relation model eager loader.
 * No more than two queries have to be executed, one for the model, one for the sub-model.
 */
trait EagerRelationship
{
    /**
     * @var array Pseudo cache for loaded relations.
     */
    private $loaded_relations = [];

    /**
     * @var array Models to load.
     */
    public $relations_to_load = [];

    /**
     * @var array Loaded models.
     */
    public $relations2 = [];

    /**
     * Eager load other models (by relation?).
     *
     * @param array $models Sub-models as relations
     *
     * @return mixed
     */
    protected function with(...$models)
    {
        $this->relations_to_load = $models;

        return $this;
    }

    /**
     * Set eager relation.
     *
     * @param string $name
     * @param \Boubou\Core\Database\Result $relation Model
     *
     * @return void
     */
    protected function setRelation($name, $relation)
    {
        // $relation->eager_loaded = true; // debug Add an attribute(first)/property(get) when eager loaded.
        $this->relations2[$name] = $relation;
    }

    /**
     * Get eager relation.
     *
     * @param string $name Relation name
     *
     * @return mixed|null
     */
    protected function getRelation($name = null)
    {
        if (! empty($name) && isset($this->relations2[$name])) {
            return $this->relations2[$name];
        }

        return null;
    }

    /**
     * Get all relations.
     *
     * @return array
     */
    protected function getRelationsToLoad()
    {
        return $this->relations_to_load;
    }

    /**
     * Hydrate a model with its relation.
     *
     * @param mixed $data The main model.
     *
     * @return mixed The hydrated model.
     */
    public function loadRelations($data)
    {
        $relations_to_load = $this->getRelationsToLoad();
        if ($data->id && count($relations_to_load)) {
            foreach ($relations_to_load as $k => $model) {
                $relation = call_user_func_array([$data, $model], []);

                // TODO: Dont unset the relation to load or only the first model
                // will be filled with this. So, how to avoid multiple queries
                // for a same result? Ie. posts->with([user]).
                // loaded_relations?

                if (! count($relation->relation->foreign_keys)) {
                    $key = $model . json_encode($relation->attributes);
                } else {
                    $key = $model . json_encode($relation->relation->foreign_keys);
                }

                if (isset($this->loaded_relations[$key])) {
                    $relation = $this->loaded_relations[$key];
                } else {
                    $relation = $this->isManyRelation($model)
                        ? $relation->get()
                        : $relation->first();
                    $this->loaded_relations[$key] = $relation;
                }

                $data->setRelation($model, $relation);
            }
        }

        return $data;
    }
}
