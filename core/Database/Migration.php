<?php

namespace Boubou\Core\Database;

use Boubou\Core\CustomException;

/**
 * Execute the migration.
 */
class Migration
{
    /**
     * @var string
     */
    private static $path = __DIR__ . '/../../database';

    /**
     * @var array
     */
    private static $migrations = [];

    /**
     * Include all migration then get the object created into a list.
     *
     * @return self
     */
    public static function build()
    {
        self::includes();
        self::migrations();

        return (new self);
    }

    /**
     * Look for the created migrations
     *
     * @return self
     */
    public static function migrations()
    {
        $migrations = [];
        foreach (get_declared_classes() as $class) {
            if (is_subclass_of($class, __CLASS__)) {
                $migrations[] = $class;
            }
        }
        self::$migrations = $migrations;

        return (new self);
    }

    /**
     * Up all migrations
     *
     * @return self
     */
    public static function ups()
    {
        $migrations = self::$migrations;
        foreach ($migrations as $migration) {
            $migration = new $migration();
            $migration->up();
        }

        return (new self);
    }

    /**
     * Down all migrations
     *
     * @return self
     */
    public static function downs()
    {
        $migrations = array_reverse(self::$migrations);
        foreach ($migrations as $migration) {
            $migration = new $migration();
            $migration->down();
        }

        return (new self);
    }

    /**
     * Include migrations classes
     *
     * @return self
     */
    private static function includes()
    {
        $migrations = glob(self::$path . '/*');
        foreach ($migrations as $migration) {
            include $migration;
        }

        return (new self);
    }
}
