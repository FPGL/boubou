<?php

namespace Boubou\Core\Database;

use Boubou\Core\Database\EagerRelationship;

/**
 * Relations between two models.
 * Use by Core\Model.
 */
trait Relationship
{
    use EagerRelationship;

    /**
     * @var object
     */
    private $relation = null;

    /**
     * @var array
     */
    private $cache = [];

    /**
     * Define a one-to-one relationship.
     * @example $this->hasOne('App\Phone' [, 'foreign_key', ['local_key']]);
     *
     * @param string $related
     * @param string $foreign_key
     * @param string $local_key
     *
     * @return mixed Instance of the related model "$related" class.
     */
    public function hasOne($related, $foreign_key = null, $local_key = null)
    {
        $related = $this->instance($related);

        $foreign_key = $foreign_key ?: $this->getForeignKey($related);
        $local_key = $local_key ?: $this->getLocalKey();

        $data = $related->select($related->getTable() . '.*')
            ->join($this->getTable(), $foreign_key, '=', $local_key)
            ->where($local_key, '=', $this->id);

        $data->relation->relation_method = __FUNCTION__;
        $data->relation->relation_caller_model = debug_backtrace()[1]['object'];

        $fk = preg_replace('/' . $related->getTable() . './', '', $foreign_key);
        $related->relation->foreign_keys[$fk] = $this->id;

        return $related;
    }

    /**
     * Define an inverse one-to-one or many relationship.
     * @example $this->belongsTo('App\Post' [, 'foreign_key'[, 'other_key']]);
     *
     * @param string $related
     * @param string $local_key
     * @param string $other_key
     *
     * @return mixed Instance of the related model "$related" class.
     */
    public function belongsTo($related, $local_key = null, $other_key = null)
    {
        $related = $this->instance($related);

        $local_key = $local_key ?: $related->getLocalKey();
        $other_key = $other_key ?: $this->getOtherKey($related);

        $data = $related->select($related->getTable() . '.*')
            ->where($local_key, '=', $this->$other_key);

        $data->relation->relation_method = __FUNCTION__;
        $data->relation->relation_caller_model = debug_backtrace()[1]['object'];

        $fk = preg_replace('/' . $related->getTable() . './', '', $other_key);
        $related->$fk = $this->$other_key;

        return $related;
    }

    /**
     * Define a one-to-many relationship.
     * @example $this->hasMany('App\Comment' [, 'foreign_key'[, 'local_key']]);
     *
     * @param string $related
     * @param string $foreign_key
     * @param string $local_key
     *
     * @return mixed Instance of the related model "$related" class.
     */
    public function hasMany($related, $foreign_key = null, $local_key = null)
    {
        // $debug = debug_backtrace();
        // dump($debug);

        $related = $this->instance($related);

        $foreign_key = $foreign_key ?: $this->getForeignKey($related);
        $local_key = $local_key ?: $this->getLocalKey();

        $data = $related->select($related->getTable() . '.*')
            ->join($this->getTable(), $foreign_key, '=', $local_key)
            ->where($local_key, '=', $this->id);

        $data->relation->relation_method = __FUNCTION__;
        $data->relation->relation_caller_model = debug_backtrace()[1]['object'];

        $fk = preg_replace('/' . $related->getTable() . './', '', $foreign_key);
        $related->relation->foreign_keys[$fk] = $this->id;

        return $related;
    }

    /**
     * Define a many-to-many relationship.
     * @example $this->belongsToMany('App\Role' [, 'roles_users'[, 'user_id'[, 'role_id']]]);
     *
     * @param string $related
     * @param string $table
     * @param string $foreign_key
     * @param string $local_key
     *
     * @return mixed Instance of the related model "$related" class.
     */
    public function belongsToMany(
        $related,
        $table = null,
        $foreign_pivot_key = null,
        $related_pivot_key = null,
        $foreign_key = null,
        $related_key = null
    ) {
        $related = $this->instance($related);

        $table = $table ?: $this->getBelongsToManyTable($related);

        $foreign_pivot_key = $foreign_pivot_key ?: $table . '.' . $this->getOtherKey($this);
        $related_pivot_key = $related_pivot_key ?: $table . '.' . $this->getOtherKey($related);

        $foreign_key = $foreign_key ?: $related->getLocalKey();
        $related_key = $related_key ?: $this->getLocalKey();

        $data = $related->select($related->getTable() . '.*')
            ->join($table, $related_pivot_key, '=', $foreign_key)
            ->join($this->getTable(), $related_key, '=', $foreign_pivot_key)
            ->where($related_key, '=', $this->id);

        // if (empty($this->id)) {
        //     // Model not yes hydrated :(
        // }
        // dump($this->id);

        $data->relation->relation_method = __FUNCTION__;
        $data->relation->relation_caller_model = debug_backtrace()[1]['object'];

        $fk = $related_key;

        $related->relation->foreign_keys[$fk] = $this->id;

        return $related;
    }

    /**
     * @param object $instance
     *
     * @return void
     */
    private function init($instance)
    {
        if (! $instance->relation) {
            $instance->relation = (object) [
                'many_methods' => ['hasMany', 'belongsToMany'],
                'relation_method' => '',
                'foreign_keys' => []
            ];
        }
    }

    /**
     * Create a new model instance for a related model.
     *
     * @param string $class
     *
     * @return mixed Instance of the related model "$class" class.
     */
    private function instance($class)
    {
        $instance = new $class;
        $this->init($instance);

        return $instance;
    }

    /**
     * Guess if it is a "many" Relation.
     *
     * @param string $name
     *
     * @return bool
     */
    private function isManyRelation($name)
    {
        $this->init($this);

        // if (! is_object($this->relation)) {
        //     return false;
        // }

        return in_array($this->$name()->relation->relation_method, $this->relation->many_methods)
            ? true
            : false;
    }

    /**
     * Guess if a model has a relation "name".
     *
     * @param string $name Name of the relationship
     *
     * @return bool
     */
    private function hasRelation($name)
    {
        return in_array($name, get_class_methods($this))
            ? true
            : false;
    }

    /**
     * If hasRelation($name) return relation($name).
     *
     * @param string $name Name of the relationship
     *
     * @return mixed Related resources in DB.
     */
    private function relation($name)
    {
        $related = call_user_func_array([$this, $name], []);
        $method = $related->relation->relation_method;

        if (in_array($method, ['hasOne', 'hasMany'])) {
            $foreign_key = $this->getForeignKey($related);
            $id = $this->id;
        } elseif (in_array($method, ['belongsTo'])) {
            $foreign_key = $related->getLocalKey();
            $other_key = $this->getOtherKey($related);
            $id = $this->$other_key;
        } elseif (in_array($method, ['belongsToMany'])) {
            $foreign_key = $this->getLocalKey();
            $id = $this->id;
        } else {
            // dd($method);
        }

        $key = implode('_', [$related->getTable(), $method, $foreign_key]);
        if ($value = $this->getCache($key)) {
            return $value;
        }

        $value = $this->isManyRelation($name)
            ? $related->where($foreign_key, '=', $id)->get()
            : $related->where($foreign_key, '=', $id)->first();
        $this->setCache($key, $value);

        return $value;
    }

    /**
     * Guess the name of a foreigh key.
     *
     * @param mixed $related The relation.
     *
     * @return string
     */
    private function getForeignKey($related)
    {
        $reflect = new \ReflectionClass($this);

        return $related->getTable() . '.' . strtolower($reflect->getShortName()) . '_id';
    }

    /**
     * Guess the name of a primary key.
     *
     * @return string
     */
    private function getLocalKey()
    {
        return $this->getTable() . '.id';
    }

    /**
     * Guess the name of a foreigh key.
     *
     * @param mixed $related The relation.
     *
     * @return string
     */
    private function getOtherKey($related)
    {
        $reflect = new \ReflectionClass($related);

        return strtolower($reflect->getShortName()) . '_id';
    }

    /**
     * Join table.
     *
     * @param mixed $related The relation.
     *
     * @return string
     */
    private function getBelongsToManyTable($related)
    {
        // role_user
        $table_a = $related->getTable();
        $table_b = $this->getTable();

        return $table_a . '_' . $table_b;
    }

    /**
     * Retrieve a cache property.
     *
     * @param string $key Name of the property.
     *
     * @return mixed
     */
    private function getCache($key)
    {
        if (isset($this->cache[$key])) {
            return $this->cache[$key];
        }
    }

    /**
     * Store a cache property.
     *
     * @param string $key Name of the property.
     * @param string $value Value of the property.
     *
     * @return void
     */
    private function setCache($key, $value)
    {
        $this->cache[$key] = $value;
    }

    /**
     * Is utilized for reading data from inaccessible properties.
     *
     * @param string $name
     *
     * @return void
     */
    public function __get($name)
    {
        dd($name);
    }
}
