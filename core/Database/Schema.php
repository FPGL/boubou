<?php

namespace Boubou\Core\Database;

use Closure;

/**
 * Schema migration.
 */
class Schema extends Database
{
    /**
     * Create a new DB table.
     *
     * @param string $table
     * @param Closure $closure
     *
     * @return void
     */
    public static function create($table, Closure $closure)
    {
        $queries = [];

        $migration = new Table();
        $closure($migration);
        $columns = $migration->getColumns();
        $columns = implode(", ", $columns);

        $sqls = $migration->getSqls();
        $queries = array_merge($queries, $sqls);

        // TODO: Sqlite WITHOUT ROWID?
        $queries[] = "CREATE TABLE $table ($columns);";

        foreach ($queries as $query) {
            self::raw($query);
        }

        $queries = implode(PHP_EOL, $queries) . PHP_EOL;

        if (php_sapi_name() == "cli") {
            echo $queries;
        }
    }

    /**
     * Drop table.
     *
     * @param string $table
     *
     * @return void
     */
    public static function drop($table)
    {
        $query = "DROP TABLE IF EXISTS $table;";
        if (php_sapi_name() == "cli") {
            echo $query . PHP_EOL;
        }
        self::raw($query);
    }
}
