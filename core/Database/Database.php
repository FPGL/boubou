<?php

namespace Boubou\Core\Database;

use Boubou\Core\Model;

/**
 * Call the methods of the model related to the database manipulation.
 */
class Database
{
    /**
     * Call static method.
     *
     * @param string $method
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic($method, $arguments)
    {
        $model = new Model;

        return call_user_func_array([$model, $method], $arguments);
    }
}
