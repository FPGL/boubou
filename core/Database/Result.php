<?php

namespace Boubou\Core\Database;

use Boubou\Core\Database\ResultPaginator;

/**
 * Orm result.
 */
class Result implements \Countable
{
    use ResultPaginator;

    /**
     * @var int Query limit (per page).
     */
    protected $limit = 0;

    /**
     * Prepare result response.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->fill($data);
    }

    /**
     * Fill the model with the following data.
     *
     * @param array $data
     *
     * @return void
     */
    public function fill($data = [])
    {
        foreach ($data as $k => $v) {
            $this->$k = $v;
        }
    }

    /**
     * Count retrieved results.
     * TODO: Interface Countable is not always firing this method.
     *
     * @return int
     */
    public function count()
    {
        $vars = get_object_vars($this);
        $less = get_class_vars(get_class($this));

        return count($vars) - count($less);
    }

    /**
     * Actual number of results.
     *
     * @return int
     */
    public function total()
    {
        // The count() can be 0 and pagination[total] a larger value
        // if we ask for a page beyond the possible number.
        if ($this->count() == 0
            && isset($this->pagination['total'])
            && $this->pagination['total'] > 1) {
            return 0;
        }

        // This is a Orm->paginate()
        if (isset($this->pagination['total'])) {
            return (int) $this->pagination['total'];
        }

        // This is a Orm->get()
        return $this->count();
    }

    /**
     * Return first element.
     *
     * @return mixed|null
     */
    public function first()
    {
        $vars = get_object_vars($this);

        return isset($vars[0]) ? $vars[0] : null;
    }

    /**
     * Query limit (per page).
     *
     * @return int
     */
    public function limit()
    {
        return $this->limit ?: 1;
    }

    /**
     * Result has $column = $value.
     * @example $user->roles()->has('Admin', 'name');
     * @example $post->categories()->has($category->id)
     *
     * @param string $value Value to find.
     * @param string $column The key that contains the value to find.
     *
     * @return bool
     */
    public function has($value, $column = 'id')
    {
        $self = new self;
        foreach ($this as $key => $property) {
            if (! property_exists($self, $key)) {
                if ($property->$column == $value) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Transform results to array.
     *
     * @return array
     */
    public function toArray()
    {
        $attributes = [];
        foreach ($this as $k => $v) {
            $tmp = [];
            if (! empty($v->attributes)) {
                $tmp = $v->attributes;
            }

            if (isset($v->relations2)) {
                // Add eager loaded sub-models to array
                foreach ($v->relations2 as $name => $model) {
                    $tmp[$name] = $model->toArray();
                }
            }

            if (count($tmp)) {
                $attributes[] = $tmp;
            }
        }

        return $attributes;
    }

    /**
     * Is run when writing data to inaccessible properties.
     *
     * @param string $name
     * @param mixed $value
     *
     * @return void
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * Is utilized for reading data from inaccessible properties.
     *
     * @param string $name
     *
     * @return mixed Attribute property or null.
     */
    public function __get($name)
    {
        if (isset($this->$name)) {
            // dd('__get', $name, $this->$name);
            return $this->$name;
        }

        return null;
    }

    /**
     * Used by View.php when constructing a model instance.
     * @example $users = (object) Boubou\Core\Database\Result::__set_state(array(...))
     *
     * @param array $data
     *
     * @return Model?
     */
    public static function __set_state($data)
    {
        $result = (new static);
        $result->fill($data);

        return $result;
    }
}
