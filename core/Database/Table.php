<?php

namespace Boubou\Core\Database;

/**
 * Prepare a migration query.
 */
class Table extends Database
{
    /**
     * @var array Each columns of the migration.
     */
    protected $columns = [];

    /**
     * @var array Additional SQL queries needed during the migration.
     */
    protected $sqls = [];

    /**
     * Primary key
     *
     * @param string $name
     *
     * @return $this
     */
    public function primary($name)
    {
        if (env('DB_CONNECTION') == 'sqlite') {
            $this->columns[] = "$name INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL";
        } else {
            $this->columns[] = "$name SERIAL PRIMARY KEY NOT NULL";
        }

        return $this;
    }

    /**
     * String column
     *
     * @param string $name
     *
     * @return $this
     */
    public function string($name)
    {
        $this->columns[] = "$name VARCHAR(255) NOT NULL";

        return $this;
    }

    /**
     * Text
     *
     * @param string $name
     *
     * @return $this
     */
    public function text($name)
    {
        $this->columns[] = "$name TEXT NOT NULL";

        return $this;
    }

    /**
     * Integer
     *
     * @param string $name
     *
     * @return $this
     */
    public function integer($name)
    {
        $this->columns[] = "$name INTEGER NOT NULL";

        return $this;
    }

    /**
     * decimal
     *
     * @param string $name
     * @param int $precision
     * @param int $scale
     *
     * @return $this
     */
    public function decimal($name, $precision, $scale)
    {
        $this->columns[] = "$name NUMERIC ($precision, $scale) NOT NULL";

        return $this;
    }

    /**
     * Boolean
     *
     * @param string $name
     *
     * @return $this
     */
    public function boolean($name)
    {
        $this->columns[] = "$name BOOLEAN NOT NULL";

        return $this;
    }

    /**
     * Foreign key
     *
     * @param string $name
     * @param string $reference_table
     * @param string $reference_column
     *
     * @return $this
     */
    public function foreign($name, $reference_table, $reference_column)
    {
        $this->columns[] = "$name INTEGER NOT NULL REFERENCES $reference_table ($reference_column)";

        return $this;
    }

    /**
     * Enum
     *
     * @param string $name
     * @param array $values
     *
     * @return $this
     */
    public function enum($name, $values)
    {
        if (env('DB_CONNECTION') == 'pgsql') {
            $this->sqls[] = "DROP TYPE IF EXISTS ${name}_type;";
            $this->sqls[] = "CREATE TYPE ${name}_type AS ENUM ('" . implode("', '", $values). "');";
            $this->columns[] = "$name ${name}_type NOT NULL";
        } else {
            // else if (env('DB_CONNECTION') == 'mysql')
            $this->columns[] = "$name ENUM('" . implode("', '", $values). "') NOT NULL";
        }

        return $this;
    }

    /**
     * Date
     *
     * @param string $name
     *
     * @return $this
     */
    public function date($name)
    {
        $this->columns[] = "$name TIMESTAMP NOT NULL";

        return $this;
    }

    /**
     * Basics dates columns
     *
     * @return $this
     */
    public function timestamps()
    {
        return $this
            ->date('created_at')->default('CURRENT_TIMESTAMP')
            ->date('updated_at')->default('CURRENT_TIMESTAMP');
    }

    /**
     * Soft Delete (if we dont want to DELETE FROM...)
     *
     * @return $this
     */
    public function softDeletes()
    {
        return $this->date('deleted_at')->nullable();
    }

    /**
     * This column is nullable
     *
     * @return $this
     */
    public function nullable()
    {
        $i = count($this->columns) - 1;
        $this->columns[$i] = preg_replace('#NOT NULL#', "NULL", $this->columns[$i]);

        return $this;
    }

    /**
     * This column can not have duplicate content
     *
     * @return $this
     */
    public function unique()
    {
        $i = count($this->columns) - 1;
        $this->columns[$i] .= " UNIQUE";

        return $this;
    }

    /**
     * Default value for this column
     *
     * @param string $value
     *
     * @return $this
     */
    public function default($value)
    {
        $i = count($this->columns) - 1;
        if ($value !== mb_strtoupper($value)) {
            $value = "'$value'";
        }
        $this->columns[$i] .= " DEFAULT $value";

        return $this;
    }

    /**
     * @param string $option CASCADE, SET NULL, SET DEFAULT, RESTRICT
     */
    public function onDelete($option)
    {
        $i = count($this->columns) - 1;
        $this->columns[$i] .= " ON DELETE $option";
    }

    /**
     * @param string $option CASCADE, SET NULL, SET DEFAULT, RESTRICT
     */
    public function onUpdate($option)
    {
        $i = count($this->columns) - 1;
        $this->columns[$i] .= " ON UPDATE $option";
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getSqls()
    {
        return $this->sqls;
    }
}
