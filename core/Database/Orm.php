<?php

namespace Boubou\Core\Database;

use Boubou\Core\CustomException;
use PDO;
use PDOException;
use PDOStatement;

/**
 * Object-relational mapping (PostgreSQL, MySQL or SQLite).
 */
trait Orm
{
    /**
     * @var object
     */
    private $orm = null;

    /**
     * Prepare the $orm and connect the DB.
     */
    public function __construct()
    {
        $this->orm = (object) [
            'pdo' => null,
            'query' => '',
            'columns' => [],
            'where' => [],
            'order' => [],
            'join' => [],
            'binded' => [],
            'pagination' => [],
        ];

        $this->connect();
    }

    /**
     * Connect to PDO.
     *
     * @return PDO
     *
     * @throws PDOException
     */
    public function connect()
    {
        extract(env('DB_*'));

        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        ];
        if (env('APP_ENV') == 'production') {
            $options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_SILENT;
        }

        if ($DB_CONNECTION == 'sqlite') {
            $path = basename(realpath('./')) == 'public' ? '../' : '';
            $path = realpath($path . $DB_DATABASE);
            $dsn = "sqlite:$path";
        } else {
            $dsn = "$DB_CONNECTION:host=$DB_HOST;port=$DB_PORT;dbname=$DB_DATABASE";
        }

        try {
            $this->orm->pdo = new PDO($dsn, $DB_USERNAME, $DB_PASSWORD, $options); // ERRMODE_EXCEPTION
        } catch (PDOException $e) {
            throw new CustomException($e->getMessage());
        }
    }

    /**
     * Execute a raw query.
     *
     * @param string $query
     * @param array $params
     *
     * @return PDOStatement
     *
     * @throws CustomException (PDOException)
     */
    protected function raw($query = '', $params = [])
    {
        $statement = $this->orm->pdo->prepare($query);
        foreach ($this->orm->binded as $bind) {
            $column = array_keys($bind)[0];
            $value = array_values($bind)[0];
            $statement->bindValue($column, $value);
        }

        try {
            $statement->execute();
        } catch (PDOException $e) {
            $message = $e->getMessage();
            throw new CustomException($message);
        }

        return $statement;
    }

    /**
     * Fetch result as object.
     *
     * @param string $query
     * @param array $params
     *
     * @return PDOStatement
     */
    protected function fetch($query, $params = [])
    {
        // dump($query, $this->orm->binded);
        $statement = $this->raw($query, $params);
        $fetch = $statement->fetchAll();
        $fetch = $this->_toModel($fetch);
        $this->clean();

        return $fetch;
    }

    /**
     * Fetch first result.
     *
     * @return Result
     */
    protected function first()
    {
        // $debug = debug_backtrace();
        // dump($debug);

        $query = $this->limit(1)->get(false);
        $result = $this->fetch($query);
        if ($result->total()) {
            $result = $result->first();
        }

        return $result;
    }

    /**
     * Fetch first result of abort with a 404.
     *
     * @return Result
     */
    public function firstOrFail()
    {
        $res = $this->first();
        if (! $res || empty($res->orm)) {
            // No result.
            abort(404, 'Not found!');
        }

        return $res;
    }

    /**
     * Fetch all matching results.
     *
     * @param bool $go True to fetch, False to return the query string.
     *
     * @return PDOStatement|Result|string
     */
    protected function get($go = true)
    {
        $table = $this->getTable();

        $columns = '*';
        if (count($this->orm->columns)) {
            $columns = implode(', ', $this->orm->columns);
        }

        $query = "SELECT $columns FROM $table";

        if (count($this->orm->join)) {
            $query .= PHP_EOL . implode(" ", $this->orm->join);
        }

        if (count($this->orm->where)) {
            $query .= PHP_EOL . "WHERE";
            foreach ($this->orm->where as $k => $v) {
                if ($k > 0) {
                    $query .= PHP_EOL . implode(" ", $v);
                } else {
                    $query .= PHP_EOL . $v[1];
                }
            }
        }

        if (count($this->orm->order)) {
            $query .= PHP_EOL . "ORDER BY " . implode(', ', $this->orm->order);
        }

        if (isset($this->orm->limit) && ! is_null($this->orm->limit)) {
            $query .= PHP_EOL . "LIMIT " . $this->orm->limit;

            if (! empty($this->orm->pagination['page'])) {
                // TODO Offset MySql
                $offset = $this->orm->limit * ($this->orm->pagination['page'] - 1);
                $query .= PHP_EOL . "OFFSET " . $offset;
            }
        }

        // echo PHP_EOL . '<pre>' . $query . '</pre>';

        if ($go) {
            $result = $this->fetch($query);

            return $result;
        }

        return $query;
    }

    /**
     * First element of the matching $id.
     *
     * @param int $id
     *
     * @return Result
     */
    protected function find($id)
    {
        $result = $this->where('id', '=', $id)->first();

        return $result;
    }

    /**
     * Return the query string.
     *
     * @return string
     */
    protected function toSql()
    {
        return $this->get(false);
    }

    /**
     * Insert a new record.
     *
     * @param array $data
     *
     * @return Result|PDOStatement
     */
    protected function create($data = [])
    {
        $table = $this->getTable();
        $fillable = $this->getFillable();

        // TODO And through a relation? Do we need to clean up?
        if (true) {
            $this->clean();
        }

        // Add missing form fields.
        // Ie.: "user_id" with $user->posts()->create(['title' => 'Hello', 'article' => 'World!']);
        if (isset($this->relation->foreign_keys)) {
            $data = array_merge($data, $this->relation->foreign_keys);
        }

        // Bind columns.
        $set = [];
        foreach ($data as $k => $v) {
            if ($fillable) {
                if ($this->_isFillable($k, $fillable, $this)) {
                    $param = $this->bindColumn($k, $v);
                    $set[$k] = ":$param";
                }
            } else {
                $param = $this->bindColumn($k, $v);
                $set[$k] = ":$param";
            }
        }
        $keys = implode(', ', array_keys($set));
        $values = implode(', ', array_values($set));

        // Prepared query.
        $query = "INSERT INTO $table ($keys) VALUES ($values);";

        $statement = $this->raw($query);
        $last_insert_id = $this->orm->pdo->lastInsertId();

        $this->clean();

        return $this->find($last_insert_id);
    }

    /**
     * Update an existing record.
     *
     * @param array $data
     *
     * @return PDOStatement
     */
    protected function update($data = [])
    {
        $table = $this->getTable();
        $fillable = $this->getFillable();

        if (isset($this->relation->foreign_keys)) {
            $data = array_merge($data, $this->relation->foreign_keys);
        }

        $set = [];
        foreach ($data as $k => $v) {
            if ($fillable) {
                if ($this->_isFillable($k, $fillable, $this)) {
                    $param = $this->bindColumn($k, $v);
                    $set[] = "$k=:$param";
                }
            } else {
                $param = $this->bindColumn($k, $v);
                $set[] = "$k=:$param";
            }
        }
        $set = implode(', ', $set);

        $id = (int )$this->id;
        $query = "UPDATE $table SET $set WHERE id=$id;";

        $statement = $this->raw($query);

        $this->clean();

        return $this->first();
    }

    /**
     * Delete an existing record.
     *
     * @return PDOStatement
     *
     * TODO Look for "deleted_at" column getColumns()?, if exists do a Soft Delete.
     * TODO Add a condition in fetch() to consider "deleted_at".
     * TODO With a "Soft Delete", a withTrashed()/restoreFromTrash() is necessary.
     */
    protected function delete()
    {
        $table = $this->getTable();

        $user = $this->select('id')->first();
        $id = (int) $user->id;
        $query = "DELETE FROM $table WHERE id=$id;";

        return $this->raw($query);
    }

    /**
     * Selected columns.
     *
     * @param mixed $columns
     *
     * @return mixed
     */
    protected function select(...$columns)
    {
        $this->orm->columns = $columns;

        return $this;
    }

    /**
     * Join two tables
     *
     * @param string $related
     * @param string $column_a
     * @param string $operator
     * @param string $column_b
     * @param string $type
     *
     * @return mixed
     */
    protected function join($related, $column_a, $operator = '', $column_b, $type = 'INNER')
    {
        // $type:
        // (INNER) JOIN: Returns records that have matching values in both tables
        // LEFT (OUTER) JOIN: Return all records from the left table, and the matched records from the right table
        // RIGHT (OUTER) JOIN: Return all records from the right table, and the matched records from the left table
        // FULL (OUTER) JOIN: Return all records when there is a match in either left or right table
        $this->orm->join[] = "$type JOIN $related ON $column_a $operator $column_b";

        return $this;
    }

    /**
     * Where clause.
     *
     * @param string $column
     * @param string $operator
     * @param mixed $value
     * @param string $boolean and|or
     *
     * @return mixed Model
     */
    protected function where($column, $operator, $value, $boolean = 'and')
    {
        $param = $this->bindColumn($column, $value);
        $this->orm->where[] = [$boolean, "$column $operator :$param"];

        return $this;
    }

    /**
     * Where clause.
     *
     * @param string $column
     * @param string $operator
     * @param mixed $value
     *
     * @return mixed Model
     */
    protected function orWhere($column, $operator, $value)
    {
        return $this->where($column, $operator, $value, 'or');
    }

    /**
     * Order by.
     *
     * @param string $columns
     * @param string $direction
     *
     * @return mixed Model
     */
    protected function orderBy($column, $direction = 'ASC')
    {
        $this->orm->order[] = "$column $direction";

        return $this;
    }

    /**
     * Limit number of query result.
     *
     * @param int $limit
     */
    protected function limit(int $value)
    {
        $this->orm->limit = $value;

        return $this;
    }

    /**
     * Bind PDO column.
     *
     * @param string $column
     * @param string $value
     *
     * @return string
     */
    protected function bindColumn($column, $value)
    {
        $column = preg_replace('#\.#', '_', $column);
        $this->orm->binded[] = [':' . $column => $value];

        return $column;
    }

    /**
     * Count SQL raws.
     *
     * @return int
     */
    public function length()
    {
        $fillable = $this->getFillable();

        $data = [];
        if (isset($this->relation->foreign_keys)) {
            $data = $this->relation->foreign_keys;
        }

        $old_order = $this->orm->order;
        $old_columns = $this->orm->columns;
        $this->orm->order = [];
        $this->orm->columns = [];
        $query = $this->select('COUNT(*) AS total');
        foreach ($data as $k => $v) {
            if ($fillable) {
                if ($this->_isFillable($k, $fillable, $this)) {
                    $this->where($k, '=', $v);
                }
            } else {
                $this->where($k, '=', $v);
            }
        }
        $fetch = $query->first();
        $total = 0;

        if (! is_null($fetch)) {
            $total = $fetch->total;
        }

        $this->orm->order = $old_order;
        $this->orm->columns = $old_columns;

        return $total;
    }

    /**
     * $model->name = 'toto';
     * $model->save()
     */
    public function save()
    {
        return $this->update($this->attributes);
    }

    /**
     * Sync a join table (Ie.: with a belongsToMany relation).
     *
     * @param array $ids
     */
    public function sync($ids = [])
    {
        // May be inverted: categories_posts | posts_categories
        $model = $this->relation->relation_caller_model;
        $related = $this;

        // Pivot table and keys
        $table = $model->getBelongsToManyTable($related);
        $foreign_pivot_key = $model->getOtherKey($model);
        $related_pivot_key = $model->getOtherKey($related);

        // Previous results
        $query = "SELECT id, $foreign_pivot_key, $related_pivot_key FROM $table WHERE $foreign_pivot_key='{$model->id}';";
        $statement = $this->orm->pdo->prepare($query);
        try {
            $statement->execute();
        } catch (PDOException $e) {
            $message = $e->getMessage();
            throw new CustomException($message);
        }
        $olds = $statement->fetchAll();

        foreach ($ids as $k => $id) {
            if (empty($id)) {
                unset($ids[$k]);
            }
        }

        $ids_olds = [];
        foreach ($olds as $old) {
            $ids_olds[] = (int) $old->$related_pivot_key;
        }

        $ids_to_remove = array_diff($ids_olds, $ids);
        $ids_to_insert = array_diff($ids, $ids_olds);

        // Remove
        if (count($ids_to_remove)) {
            $query = "DELETE FROM $table WHERE $foreign_pivot_key={$model->id} AND $related_pivot_key IN ('" . implode("', '", $ids_to_remove) . "');";
            $statement = $this->orm->pdo->prepare($query);
            $statement->execute();
        }

        // New
        // Inserts
        if (count($ids_to_insert)) {
            $data = [];
            foreach ($ids_to_insert as $id) {
                $query = "INSERT INTO $table ($foreign_pivot_key, $related_pivot_key) VALUES ('{$model->id}', '$id');";
                $statement = $this->orm->pdo->prepare($query);
                $statement->execute();
            }
        }
    }

    /**
     * Return result ready to paginate.
     *
     * @param int $per_page Number of result per page.
     *
     * @return Result
     */
    public function paginate($per_page = null)
    {
        $per_page = $this->perPage($per_page);
        $orm = clone $this->orm;
        $length = $this->length();

        $this->orm = $orm;
        $this->orm->pagination['page'] = (int) (request()->page ?? 1);
        $this->orm->pagination['total'] = $length;

        return $this->limit($per_page)->get();
    }

    /**
     * Number of result per page (@see config.php).
     *
     * @param int $per_page
     *
     * @return int
     */
    public function perPage($per_page)
    {
        return $per_page ?: config('pagination')['per_page'];
    }

    /**
     * Empty the $orm for the next query.
     *
     * @return void
     */
    protected function clean()
    {
        $this->orm->query = '';
        $this->orm->columns = [];
        $this->orm->where = [];
        $this->orm->order = [];
        $this->orm->join = [];
        $this->orm->limit = null;
        $this->orm->binded = [];
        $this->orm->pagination = [];
    }

    /**
     * Fill the model with the DB result.
     *
     * @param array $fetch
     *
     * @return mixed The model
     */
    private function _toModel($fetch)
    {
        $model = (new static);

        foreach ($fetch as $k => $data) {
            $model = new $model;
            $model->fill($data);
            $fetch[$k] = $model;
        }

        $result = new Result($fetch);
        $result->setPagination($this->orm->pagination, isset($this->orm->limit) ? $this->orm->limit : 0);

        // Eager load sub-models relations
        foreach ($result as $k => $model) {
            $this->loadRelations($model);
        }

        return $result;
    }

    /**
     * Does a column is fillable by user.
     *
     * @param string $k Column name
     * @param array $fillable Column list
     * @param mixed $el Model
     *
     * @return bool
     */
    private function _isFillable($k, $fillable, $el)
    {
        return in_array($k, $fillable)
            || ($el->relation && in_array($k, array_keys($el->relation->foreign_keys)));
    }

    /**
     * To access these protected methods.
     *
     * @param string $method
     * @param mixed $arguments
     *
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return call_user_func_array([$this, $method], $arguments);
    }
}
