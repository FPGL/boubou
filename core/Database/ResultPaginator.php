<?php

namespace Boubou\Core\Database;

/**
 * Pagination of the Orm result.
 */
trait ResultPaginator
{
    /**
     * @var array The pagination object (page, total).
     */
    private $pagination = null;

    /**
     * @var int Number of bullets for the pagination (@see config.php).
     */
    protected $bullets_max = 5;

    /**
     * Set the model to remember the context of the query.
     *
     * @param array $pagination
     *
     * @return void
     */
    public function setPagination($pagination = [], $limit = 0)
    {
        $this->pagination = $pagination;
        $this->limit = $limit;
    }

    /**
     * Current page.
     *
     * @return int
     */
    public function currentPage()
    {
        return $this->pagination['page'] ?? 1;
    }

    /**
     * Total number of query raws.
     *
     * @return int
     */
    public function totalPages()
    {
        return $this->pagination['total'] ?? 0;
    }

    /**
     * How many pages.
     *
     * @return int
     */
    public function numberOfPages()
    {
        $total = $this->totalPages();
        $limit = $this->limit();

        return (int) ceil($total / $limit);
    }

    /**
     * Start number of bullet.
     *
     * @return int
     */
    public function startPage()
    {
        $page = $this->currentPage();

        return max(1, $page - floor($this->bulletsMax() / 2));
    }

    /**
     * End number of bullet.
     *
     * @return int
     */
    public function maxPage()
    {
        $start = $this->startPage();

        return $start + $this->bulletsMax() - 1;
    }

    /**
     * Previous arrow.
     *
     * @return int
     */
    public function previousPage()
    {
        $page = $this->currentPage();

        return max(1, $page - 1);
    }

    /**
     * Next arrow.
     *
     * @return int
     */
    public function nextPage()
    {
        $number_of_pages = $this->numberOfPages();
        $page = $this->currentPage();

        return min($number_of_pages, $page + 1);
    }

    /**
     * Bullets max for pagibation.
     *
     * @return int
     */
    public function bulletsMax()
    {
        return config('pagination')['bullets_max'] ?: $this->bullets_max;
    }
}
