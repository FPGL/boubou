<?php

namespace Boubou\Core;

/**
 * File manipulation.
 */
class File
{
    /**
     * @var string
     */
    private $storage_path = __DIR__ . '/../storage/uploads';

    /**
     * @var array
     */
    private $attributes = [];

    /**
     * @var int Max file size.
     */
    private $max_filesize = '2M'; // 2M = 2097152

    /**
     * @var array Default allowed mime types to upload (@see config.php).
     */
    public $allowed_mimes = [
        'jpg|jpeg|jpe' => 'image/jpeg',
        'gif' => 'image/gif',
        'png' => 'image/png',
    ];


    /**
     * Populate the file.
     */
    public function __construct($file = null)
    {
        $this->storage_path = realpath($this->storage_path);
        $this->max_filesize = $this->getMaxFilesize();

        if ($file && $file['tmp_name']) {
            foreach ($file as $k => $v) {
                $this->set($k, $v);
            }
            if (! $this->size || $this->size >= $this->max_filesize) {
                abort(431, 'Max filesize reached? (' . $this->max_filesize . ')');
            }
        }
    }

    /**
     * Move file to destination.
     *
     * @param string $name
     *
     * @return bool True if uploaded.
     */
    public function moveToStorage($name = null)
    {
        if (! $this->mimeTypeIsAllowed()) {
            abort(500, 'Mime type (' . $this->type . ') not allowed!');
        }

        $name = $name ?: $this->name;
        $filename = $this->filename($name);
        $extension = $this->extension($name);

        $target = $this->storage_path . '/' . slug($filename) . '.' . $extension;

        return move_uploaded_file($this->tmp_name, $target);
    }

    /**
     * Unlink a file.
     *
     * @return void
     */
    public static function unlinkFromStorage($name)
    {
        $self = (new self);
        $filename = $self->filename($name);
        $extension = $self->extension($name);
        $target = $self->storage_path . '/' . slug($filename) . '.' . $extension;
        if ($self->exists($target)) {
            unlink($target);
        }
    }

    /**
     * Full path from storage.
     *
     * @param string $name
     *
     * @return string
     */
    public static function getPathFromStorage($name)
    {
        $self = (new self);
        $filename = $self->filename($name);
        $extension = $self->extension($name);
        $target = $self->storage_path . '/' . slug($filename) . '.' . $extension;
        // $target = preg_replace('#' . preg_quote(realpath(__DIR__ . '/..')) . '#', '', $target);

        return $target;
    }

    /**
     * Return the requested file as array.
     *
     * @return array
     */
    public function all()
    {
        return (array) $this->attributes;
    }

    /**
     * Check if file already exists.
     *
     * @return bool
     */
    public function exists($target)
    {
        return file_exists($target);
    }

    /**
     * Path info extention.
     *
     * @return string
     */
    public function extension($name = null)
    {
        $name = $name ?: $this->name;

        return strtolower(pathinfo($name, PATHINFO_EXTENSION));
    }

    /**
     * Do this extension is allowed to upload.
     *
     * @param string
     *
     * @return bool
     */
    public function mimeTypeIsAllowed()
    {
        $allowed_mimes = config('allowed_mimes') ?? $this->allowed_mimes;
        $keys = array_keys($allowed_mimes);
        $values = array_values($allowed_mimes);

        $allowed_mimes = [];
        foreach ($keys as $key => $ori) {
            $ori = explode('|', $ori);
            foreach ($ori as $new) {
                $allowed_mimes[$new] = $values[$key];
            }
        }

        $extension = $this->extension();

        return isset($allowed_mimes[$extension])
            && $allowed_mimes[$extension] == $this->type;
    }

    /**
     * Path info filename.
     *
     * @return string
     */
    public function filename($name = null)
    {
        $name = $name ?: $this->name;

        return strtolower(pathinfo($name, PATHINFO_FILENAME));
    }

    /**
     * Get max filesize (in byte) form config.
     *
     * @return int
     */
    public function getMaxFilesize()
    {
        $value = ini_get('uploadmax_filesize');
        // $value = trim($value);

        if (is_numeric($value)) {
            return $value;
        }

        if (! $value) {
            $value = '2M';
        }

        $unit = strtolower($value[strlen($value)-1]);
        $value = substr($value, 0, -1);

        switch ($unit) {
            case 'g':
                $value *= 1024;
                // no break
            case 'm':
                $value *= 1024;
                // no break
            case 'k':
                $value *= 1024;
        }

        return $value;
    }

    /**
     * Write data to an "attribute" property.
     *
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * Get data from "attribute" property.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function get($name)
    {
        return $this->attributes[$name] ?? null;
    }

    /**
     * Is run when writing data to inaccessible properties.
     *
     * @param string $name
     * @param mixed $value
     *
     * @return void
     */
    public function __set($name, $value)
    {
        $this->set($name, $value);
    }

    /**
     * Is utilized for reading data from inaccessible properties.
     *
     * @param string $name
     *
     * @return mixed Attribute property.
     */
    public function __get($name)
    {
        return $this->get($name);
    }
}
