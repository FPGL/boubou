<?php

namespace Boubou\Core;

/**
 * Templates syntax.
 */
class Template
{
    /**
     * @var string Views directory.
     */
    private static $basedir = __DIR__ . '/./../resources/views/'; // DIRECTORY_SEPARATOR?

    /**
     * @var string
     */
    private static $layout = '';

    /**
     * @var array
     */
    private static $templates = [];

    /**
     * @var array
     */
    private static $sections = [];

    /**
     *
     *
     * @param string $php
     *
     * @return string $php
     */
    public static function build($php)
    {
        self::partials($php);

        foreach (self::$templates as $k => $template) {
            self::$templates[$k] = self::clearSections($template);
        }

        // Loop twice to inject the nested templates yields/includes and includes/yields.
        for ($i = 0; $i < 2; $i++) {
            foreach (self::$templates as $k => $template) {
                self::$templates[$k] = self::replaceYields($template, self::$sections);
            }
            foreach (self::$templates as $k => $template) {
                self::$templates[$k] = self::replaceIncludes($template, self::$templates);
            }
        }

        if (self::$layout) {
            // Inject fragment to its layout.
            $php = self::$templates[self::$layout];
        }

        // Replace the remaining fragments in the final template.
        $php = self::replaceIncludes($php, self::$templates);
        $php = self::stripTags($php);
        $php = self::replaceBlockTags($php);
        $php = self::replaceCurlyBrackets($php);
        $php = self::replaceCounts($php);

        // Empty the layout variable for the next render.
        self::$layout = '';

        return $php;
    }

    /**
     * Look for all files (layout and includes) and sections.
     *
     * @param string $php
     *
     * @return array
     */
    private static function partials($php)
    {
        $layout = self::getLayout($php);
        if ($layout) {
            self::partials($layout);
        }

        $includes = self::getIncludes($php);
        if ($includes) {
            foreach ($includes as $include) {
                self::partials($include);
            }
        }

        self::getSections($php);
    }

    /**
     * Look for all includes.
     *
     * @param string $php
     *
     * @return array
     */
    private static function getIncludes($php)
    {
        $includes = [];
        preg_match_all('#@include\((.*)\)#', $php, $regs);
        if (count($regs[1])) {
            foreach ($regs[1] as $k => $v) {
                $args = self::includeStringToArgs($v);
                $regs[1] = [$args[0]];
                foreach ($regs[1] as $k => $v2) {
                    $path = $v2;
                    $path = preg_replace('#[\'"]#', '', $path);
                    $includes[$path] = self::template($path);
                }
            }
        }

        return $includes;
    }

    /**
     * Replace all includes.
     *
     * @param string $php The current fragment of template.
     * @param array $templates Content of the includes.
     *
     * @return string $php
     */
    private static function replaceIncludes($php, $templates)
    {
        preg_match_all('#@include\((.*)\)#', $php, $regs);
        if (count($regs[0])) {
            foreach ($regs[1] as $k => $v) {
                $old = $v;
                $args = self::includeStringToArgs($v);
                $v = $args[0];

                $path = $v;
                $path = self::path($path);
                if (! isset($templates[$path])) {
                    throw new CustomeException("File not found: $path");
                }
                $replace = $templates[$path];


                $pattern = '@include(' . $old . ')';
                $pattern = preg_quote($pattern);
                array_shift($args);
                if (count($args)) {
                    // Additionals argument sent to @include.
                    // Ie.: @include('partials.pagination', ['list' => $posts])
                    $replace = '<?php extract(' . implode(', ', $args) . '); ?>' . $replace;
                }
                $php = preg_replace('#' . $pattern . '#', $replace, $php);
            }
        }

        return $php;
    }

    /**
     * Extends with layout.
     *
     * @param string $php
     *
     * @return string
     */
    private static function getLayout($php)
    {
        preg_match('#@extends\((.*?)\)#', $php, $regs);
        if (count($regs)) {
            self::$layout = self::path($regs[1]);

            return self::template($regs[1]);
        }

        return '';
    }

    /**
     * Sections.
     *
     * @param string $php
     */
    private static function getSections($php)
    {
        preg_match_all('#@section\((.*?)\)(.*?)@endsection#s', $php, $regs);
        array_shift($regs);
        foreach ($regs[0] as $k => $v) {
            $key = $v;
            $key = preg_replace('#[\'"]#', '', $key);
            $value = $regs[1][$k];
            self::$sections[$key] = $value;
        }
    }

    /**
     * Remove useless sections tags, already collected with self::partials().
     *
     * @param string $php
     *
     * @return string $php
     */
    private static function clearSections($php)
    {
        $php = preg_replace('#@section\(.*\)(.*)@endsection#msU', '', $php);

        return $php;
    }

    /**
     * Yields.
     *
     * @param string $php
     * @param array $sections
     *
     * @return string $php
     */
    private static function replaceYields($php, $sections = [])
    {
        preg_match_all('#@yield\((.*)\)#', $php, $regs);
        if (count($regs)) {
            $yields = preg_replace('#[\'"]#', '', $regs[1]);

            foreach ($yields as $k => $v) {
                if (isset($sections[$v])) {
                    $pattern = '@yield(\'' . $v . '\')';
                    $pattern = preg_quote($pattern);
                    $replace = $sections[$v];
                    $php = preg_replace('#' . $pattern . '#', $replace, $php);
                }
            }
        }

        return $php;
    }

    /**
     * Replace block tags.
     *
     * @param string $php
     *
     * @return string $php
     */
    private static function replaceBlockTags($php)
    {
        $patterns = [
            '#@if([\s]+)?\((.*)\)#' => '<?php if ($2): ?>',
            '#@else#' => '<?php else: ?>',
            '#@endif#' => '<?php endif; ?>',
            '#@foreach([\s]+)?\((.*)\)#' => '<?php foreach ($2): ?>',
            '#@endforeach#' => '<?php endforeach; ?>',
            '#@for([\s]+)?\((.*)\)#' => '<?php for ($2): ?>',
            '#@endfor#' => '<?php endfor; ?>',
        ];

        $php = preg_replace(array_keys($patterns), array_values($patterns), $php);

        return $php;
    }

    /**
     * Replace curly brackets.
     *
     * @param string $php
     *
     * @return string
     */
    private static function replaceCurlyBrackets($php)
    {
        //  | ENT_HTML5?
        $php = preg_replace('#\{\{([\s]+)?\$(.*?)([\s]+)?\}\}#', '<?php echo htmlspecialchars($$2, ENT_QUOTES, \'UTF-8\'); ?>', $php);
        $php = preg_replace('#\{\{([\s]+)?([a-zA-Z_:!\(\)\->])(.*?)([\s]+)?\}\}#', '<?php echo htmlspecialchars($2$3, ENT_QUOTES, \'UTF-8\'); ?>', $php);
        $php = preg_replace_callback('#\{!!([\s]+)?\$(.*?)([\s]+)?!!\}#', function ($matches) {
            return '<?php echo \Boubou\Core\Template::sanitize($' . $matches[2] . '); ?>';
        }, $php);
        $php = preg_replace_callback('#\{!!(([\s]+)?([a-zA-Z_:\(\)\->])(.*?)([\s]+))?!!\}#', function ($matches) {
            return '<?php echo \Boubou\Core\Template::sanitize(' . $matches[1] . '); ?>';
        }, $php);

        return $php;
    }

    /**
     * To count Coutable compacted result.
     *
     * @param string $php
     *
     * @return string
     */
    private static function replaceCounts($php)
    {
        $php = preg_replace('#count\((\$[a-z]+)\)#', 'count((array) $1)', $php);

        return $php;
    }

    /**
     * Strip HTML comments
     *
     * @param string $php
     *
     * @return string $php
     */
    public static function stripComments($php)
    {
        $php = preg_replace('#<!--.*?-->#s', '', $php);
        $php = preg_replace('#{{--.*?--}}#s', '', $php);

        return $php;
    }

    /**
     * Remove the blade tags that have become useless.
     *
     * @param string $php
     *
     * @return string $php
     */
    private static function stripTags($php)
    {
        $php = preg_replace('#@extends\((.*?)\)#', '', $php);
        $php = preg_replace('#@yield\((.*?)\)#', '', $php);
        $php = preg_replace('#@include\((.*?)\)#', '', $php);
        $php = preg_replace('#@section\(.*\)(.*)@endsection#msU', '$1', $php);

        return $php;
    }

    /**
     * Path to the view.
     *
     * @param string $path
     *
     * @return string
     */
    private static function path($path)
    {
        $path = preg_replace('#[\'"]#', '', $path);
        $path = preg_replace('#\.#', '/', $path);
        $realpath = realpath(self::$basedir . $path . '.blade.php');

        if (! $realpath) {
            throw new \Exception($path . '.blade.php not found!', 1);
        }

        return $realpath;
    }

    /**
     * Get the source template.
     *
     * @param string $path
     *
     * @return string
     */
    private static function template($path)
    {
        $path = self::path($path);
        $content = file_get_contents($path);
        $content = self::stripComments($content);
        self::$templates[$path] = $content;

        return $content;
    }

    /**
     * To dissociate view path from additionals argument sent to the @include.
     * @example @include('partials.pagination', ['list' => $posts])
     *
     * @param string $s For example: 'partials.pagination', ['list' => $posts]
     *
     * @return array
     */
    private static function includeStringToArgs($s)
    {
        $s = preg_replace_callback(
            '|"[^"]+"|',
            function ($matches) {
                return str_replace('\'', '\'', '\'*comma*\'', $matches[0]);
            },
            $s
        );
        $args = explode(',', $s);
        $args = str_replace('*comma*', ',', $args);

        return $args;
    }

    /**
     * Secure XSS. Clear php/script/style tags from user input.
     * @example {!! nl2br($post->article) !!}
     *
     * @param string $s
     */
    public static function sanitize($s)
    {
        $s = preg_replace(['#<[\s]+script#', '#script[\s]+>#'], ['<script', 'script>'], $s);
        $s = preg_replace(['#<[\s]+style#', '#style[\s]+>#'], ['<style', 'style>'], $s);
        $s = preg_replace_callback('#(<script.*?script>)#is', function ($matches) {
            return htmlspecialchars($matches[1], ENT_QUOTES, 'UTF-8');
        }, $s);
        $s = preg_replace_callback('#(<style.*?style>)#is', function ($matches) {
            return htmlspecialchars($matches[1], ENT_QUOTES, 'UTF-8');
        }, $s);
        $s = preg_replace_callback('#(<\?.*?\?>)#is', function ($matches) {
            return htmlspecialchars($matches[1], ENT_QUOTES, 'UTF-8');
        }, $s);

        return $s;
    }
}
