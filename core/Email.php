<?php

namespace Boubou\Core;

use Boubou\Core\Log;

/**
 * Mailer.
 * @deprecated Sending mail with only php/sendmail = direct in spam.
 * @example Email::to('toto@gmail.com')->to('tata@gmail.com')->subject('Wesh world!')->message('Wesh!')->send();
 */
class Email
{
    /**
     * @var array
     */
    private static $to = [];

    /**
     * @var array
     */
    private static $from = [];

    /**
     * @var array
     */
    private static $cc = [];

    /**
     * @var array
     */
    private static $bcc = [];

    /**
     * @var string
     */
    private static $subject = '';

    /**
     * @var string
     */
    private static $message = '';

    /**
     * @var array The last email we tried to send.
     */
    private static $last = [];

    /**
     * @var array Errors bag.
     */
    private static $errors = [];

    /**
     * Set "to".
     *
     * @param string $email
     * @param string $name
     *
     * @return self
     */
    public static function to($email, $name = '')
    {
        if (self::isValid($email)) {
            self::$to[] = [$email, $name];
        }

        return (new self);
    }

    /**
     * Set "from".
     *
     * @param string $email
     * @param string $name
     *
     * @return self
     */
    public function from($email, $name)
    {
        if (self::isValid($email)) {
            self::$from = [$email, $name];
        }

        return (new self);
    }

    /**
     * Set carbon copy.
     *
     * @param string $email
     * @param string $name
     *
     * @return self
     */
    public function cc($email, $name = '')
    {
        if (self::isValid($email)) {
            self::$cc[] = [$email, $name];
        }

        return (new self);
    }

    /**
     * Set blind carbon copy.
     *
     * @param string $email
     * @param string $name
     *
     * @return self
     */
    public function bcc($email, $name = '')
    {
        if (self::isValid($email)) {
            self::$bcc[] = [$email, $name];
        }

        return (new self);
    }

    /**
     * Set "subject".
     *
     * @param string $email Mail's subject.
     *
     * @return self
     */
    public function subject($subject)
    {
        self::$subject = $subject;

        return (new self);
    }

    /**
     * Set "message".
     *
     * @param string $message Content body.
     *
     * @return self
     */
    public function message($message)
    {
        self::$message = $message;

        return (new self);
    }

    /**
     * Join an attachment.
     */
    public function attach()
    {
    }

    /**
     * Send mail.
     *
     * @param string $template Template src.
     *
     * @return bool
     */
    public function send($template = null)
    {
        $to = self::map(self::$to);
        $subject = self::$subject;
        $message = self::$message;

        $additional_headers = self::additionalHeaders();
        // $additional_headers = implode("\r\n", $additional_headers);

        // The additional_parameters parameter is disabled in safe_mode
        // and the mail() function will expose a warning message and return
        // FALSE when used.
        $additional_parameters = self::additionalParameters();

        // dd([$to, $subject, $message, $additional_headers, $additional_parameters]);

        self::$last = [$to, $subject, $message, $additional_headers, $additional_parameters];

        if (env('MAIL_DRIVER') == 'smtp') {
            return self::sendSmtp($to, $subject, $message, $additional_headers);
        } else {
            $to = implode(', ', $to);
            if (! empty($additional_headers['Cc'])) {
                $additional_headers['Cc'] = implode(', ', $additional_headers['Cc']);
            }
            if (! empty($additional_headers['Bcc'])) {
                $additional_headers['Bcc'] = implode(', ', $additional_headers['Bcc']);
            }

            return mail($to, $subject, $message, $additional_headers);
        }
    }

    /**
     * SMTP commands.
     *
     * @param string $to
     * @param string $subject
     * @param string $message
     * @param array $additional_headers
     *
     * @return bool
     */
    public static function sendSmtp($to, $subject, $message, $additional_headers = [])
    {
        $user = env('MAIL_USERNAME');
        $pass = env('MAIL_PASSWORD');
        $host = env('MAIL_HOST');
        $port = env('MAIL_PORT');

        $headers = [];
        foreach ($additional_headers as $k => $header) {
            if ($k != 'Bcc') {
                if (is_array($header)) {
                    foreach ($header as $k2 => $v2) {
                        $headers[] = $k . ': ' . $v2;
                    }
                } else {
                    $headers[] = $k . ': ' . $header;
                }
            }
        }
        $headers = implode("\r\n", $headers);

        $scheme = '';
        if (env('MAIL_ENCRYPTION') == 'tls' || env('MAIL_ENCRYPTION') == 'ssl') {
            $scheme = 'ssl://';
        }

        if (! ($socket = fsockopen($scheme . $host, $port, $errno, $errstr, 15))) {
            self::$errors[] = "Error connecting to '$host' ($errno) ($errstr)";
        }

        self::serverParseError($socket, '220');

        fwrite($socket, 'EHLO ' . $host."\r\n"); // EHLO (Extended Hello)
        self::serverParseError($socket, '250', 'EHLO ');

        fwrite($socket, 'AUTH LOGIN' . "\r\n"); // AUTH (Authentication)
        self::serverParseError($socket, '334', 'AUTH LOGIN');

        fwrite($socket, base64_encode($user)."\r\n");
        self::serverParseError($socket, '334', 'user');

        fwrite($socket, base64_encode($pass)."\r\n");
        self::serverParseError($socket, '235', 'pwd');

        fwrite($socket, 'MAIL FROM: <' . self::extractMail($additional_headers['From']) . ">\r\n");
        self::serverParseError($socket, '250', 'MAIL FROM: ' . self::extractMail($additional_headers['From']));

        foreach ($to as $email) {
            fwrite($socket, 'RCPT TO: <' . self::extractMail($email) . ">\r\n");
            self::serverParseError($socket, '250', 'RCPT TO:');
        }

        if (isset($additional_headers['Bcc'])) {
            foreach ($additional_headers['Bcc'] as $email) {
                fwrite($socket, 'RCPT TO: <' . self::extractMail($email) . ">\r\n");
                self::serverParseError($socket, '250', 'RCPT TO:');
            }
        }

        fwrite($socket, 'DATA'."\r\n");
        self::serverParseError($socket, '354', 'DATA');
        fwrite($socket, 'Subject: ' . $subject . "\r\n"
            . 'To: ' . implode(', ', $to) . "\r\n"
            . $headers . "\r\n\r\n"
            . $message . "\r\n");
        fwrite($socket, '.' . "\r\n");
        self::serverParseError($socket, '250');

        fwrite($socket, 'QUIT'."\r\n");
        fclose($socket);

        return ! count(self::$errors);
    }

    /**
     * Test an address
     *
     * @return bool
     */
    public static function isValid($email)
    {
        return preg_match('/^[-+\\.0-9=a-z_]+@([-0-9a-z]+\\.)+([0-9a-z]){2,4}$/i', $email);
    }

    /**
     * Get the errors bag.
     *
     * @return array
     */
    public static function getErrors()
    {
        return self::$errors;
    }

    /**
     * Get the last email we tried to send.
     *
     * @return array
     */
    public static function getLast()
    {
        return self::$last;
    }

    /**
     * Additional headers.
     *
     * @return array
     */
    private function additionalHeaders()
    {
        $headers = [];

        if (! count(self::$from)) {
            self::from(env('MAIL_USERNAME'), env('APP_NAME'));
        }

        $from = ! empty(self::$from[1])
            ? self::$from[1] . ' <' . self::$from[0] . '>'
            : self::$from[0];
        $cc = self::map(self::$cc);
        $bcc = self::map(self::$bcc);

        $headers['MIME-Version'] = '1.0';
        $headers['Content-type'] = 'text/html';
        $headers['charset'] = 'iso-8859-1';

        $headers['From'] = $from;
        $headers['Reply-To'] = $from;
        if ($cc) {
            $headers['Cc'] = $cc;
        }
        if ($bcc) {
            $headers['Bcc'] = $bcc;
        }

        return $headers;
    }

    /**
     * Additional parameters.
     *
     * @return array
     */
    private function additionalParameters()
    {
        return [];
    }

    /**
     * Function to processes server response codes.
     *
     * @param stream $socket Stream ressource
     * @param string $expected_response Code number
     *
     * @return void
     */
    private static function serverParseError($socket, $expected_response, $s = '')
    {
        $server_response = '';
        while (substr($server_response, 3, 1) != ' ') {
            if (! ($server_response = fgets($socket, 256))) {
                self::$errors[] = 'Error while fetching server response codes.';
            }
        }

        if (! (substr($server_response, 0, 3) == $expected_response)) {
            self::$errors[] = 'Unable to send e-mail."' . $server_response . '"';
        }

        if (count(self::$errors)) {
            Log::error($s, self::$errors);
        }
    }

    /**
     * Get the mail address "mail@example.com" from "Toto <mail@example.com>".
     *
     * @param string $s
     *
     * @return string
     */
    private static function extractMail($s)
    {
        return preg_replace('/.*<(.*?)>/', '$1', $s);
    }

    /**
     * Transform an array of address to valid recipient: "Name" <address>.
     *
     * @param array $addresses
     *
     * @return mixed
     */
    private function map(array $addresses)
    {
        $a = [];
        foreach ($addresses as $address) {
            if (! empty($address[1])) {
                $a[] = $address[1] . ' <' . $address[0] . '>';
            } else {
                $a[] = $address[0];
            }
        }

        return count($a) ? $a : null;
    }

    /**
     * To access these protected methods.
     *
     * @param string $method
     * @param mixed $arguments
     *
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return call_user_func_array([new static, $method], $arguments);
    }
}
