<?php

namespace Boubou\Core;

/**
 * Dump variables for debug purpose.
 * @see Xdebug.
 */
class Dumper
{
    /**
     * @var bool Output allow HTML (ie. browser output), or not (ie. CLI).
     */
    private static $allowhtml = false;

    /**
     * @var string CSS for the output element.
     */
    private static $styles = 'background-color:white;color: black;border: 1px solid #ccc; padding: 10px 10px 12px 12px; display: inline-block; clear: both; font: 12px monospace; line-height: 1.5em; word-wrap: break-word; word-break: break-all;';

    /**
     * Dumper.
     *
     * @param mixed $var
     *
     * @return string
     */
    private static function render($var)
    {
        $allowhtml = self::$allowhtml;

        ob_start();
        var_dump($var);
        $s = ob_get_contents();
        ob_end_clean();

        $s = str_replace("]=>\n", '] = ', $s);
        $s = preg_replace('/= {2,}/', '= ', $s);
        $s = preg_replace('/array\((.*)\) \{(.*)\}/Uis', 'array($1) [$2] ', $s);
        $s = preg_replace('/object\((.*)\)#([0-9]+) \(([0-9]+)\)/Uis', 'class $1 ($3)', $s);
        $s = preg_replace('/\[(.*password.*)] = string\(([0-9]+)\) "(.*)"/', '[$1] = string($2) <span style="color:#ff0000">*** hidden ***</span>', $s);

        $s = preg_replace_callback(
               '/string\(([0-9]+)\) "(.*)"/',
               function ($matches) use ($allowhtml) {
                   if (! $allowhtml) {
                       $matches[2] = htmlentities($matches[2]);
                   }
                   if ($matches[2] != '') {
                       $matches[2] = '<span style="color:#bbb">' . $matches[2] . '</span>';
                   }
                   return 'string(' . $matches[1] . ') "' . $matches[2] . '"';
               },
               $s
           );
        $s = preg_replace('/class ([-0-9a-zA-Z\.\\\]+) \(([0-9]+)\)/', '<b style="color:#46C3FF">class</b> <b style="color:#c400ff">$1</b>(<span style="color:#FF8400">$2</span>)', $s);

        $s = preg_replace_callback(
            // array|float|int|string|resource|object
            '/([a-z\\\_]+)\(([-0-9a-zA-Z\+\.\\\]+)\)( (.*?))?/',
            function ($regs) {
                if ($regs[1] != 'string' && empty($regs[3])) {
                    $regs[3] = $regs[2];
                }
                $tag = '<span style="color:%s">%s</span>';

                return (! empty($regs[1]) ? sprintf($tag, '#46C3FF', trim($regs[1])) : '')
                        . ($regs[2] != '' ? '(' . sprintf($tag, '#FF8400', trim($regs[2])) . ') ' : '')
                        . (! empty(trim($regs[3])) ? sprintf($tag, '#32CD32', trim($regs[3])) : '');
            },
            $s
        );

        $tag = '<span style="color:%s">%s</span>';

        $s = preg_replace("/\[\"(.*?)\"\] = /i", "[$1] = ", $s);
        $s = preg_replace('/\["([a-zA-Z_]+)":([a-z]+)\]/', '[$1:<span style="color:#c812da">$2</span>]', $s);
        $s = preg_replace('/\["([a-zA-Z_]+)":"([a-zA-Z_\\\]+)":([a-z]+)\]/', '[$1:<b style="color:#c400ff">$2</b>:<span style="color:#c812da">$3</span>]', $s);
        $s = preg_replace("/(\n[\s]{2,})\[([0-9]+)\] =/", '$1[<span style="color:#FF8400">$2</span>] =', $s);
        $s = preg_replace("/[ ]{2}/", "    ", $s);
        $s = preg_replace("/{[\s]{1,}}/", "{}", $s);
        $s = preg_replace("/\[[\s]{1,}\]/", "[]", $s);

        return $s;
    }

    /**
     * Dump.
     *
     * @example `dump(99, -98.0, 'toot', '<span style="color:#46C3FF">SPAN!</span>', ['a', 'b', -4.01], (object) ['c' => new stdClass, 'd' => 10e34], '[0] = arf', 12.343, " ", simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?>
     *   <root>
     *     <user id="1">
     *       <firstname>Pascal</firstname>
     *       <lastname>MARTIN</lastname>
     *     </user>
     *     <user id="2">
     *       <firstname>Anon</firstname>
     *       <lastname>YMOUS</lastname>
     *     </user>
     *   </root>'));`
     *
     * @param mixed $var
     * @param mixed $moreVars
     *
     * @return void
     */
    public static function dump($var, ...$moreVars)
    {
        $debug = self::file(debug_backtrace());
        $html = self::html($debug, $var, $moreVars);

        echo $html;
    }

    /**
     * Dump and die.
     * @example `dump($user, 99, -98.0, 'toot', '<span style="color:#46C3FF">SPAN!</span>', ['a', 'b', -4.01], (object) ['c' => new stdClass, 'd' => 10e34], '[0] = arf', 12.343, " ");`
     *
     * @param mixed $var
     * @param mixed $moreVars
     *
     * @return void
     */
    public static function dd($var, ...$moreVars)
    {
        $debug = self::file(debug_backtrace());
        $html = self::html($debug, $var, $moreVars);

        die($html);
    }

    /**
     * Add error css style to the dump.
     *
     * @return self
     */
    public static function isError()
    {
        self::$styles .= 'display: block; background-color: #333;color: white;';

        return (new self);
    }

    /**
     * Set HTML as allowed.
     *
     * @param $bool True if HTML is allowed. If not, HTML will be htmlentities().
     *
     * @return self
     */
    public static function allowHtml(bool $bool = false)
    {
        self::$allowhtml = $bool;

        return (new self);
    }

    /**
     * Returns the rendered HTML code.
     *
     * @param object $debug
     * @param mixed $var
     * @param mixed $moreVars
     *
     * @return string
     */
    private static function html($debug, $var, $moreVars)
    {
        if (count($moreVars[0]) > 1) {
            // TODO: Ugly! This piece of code may be necessary if a compiled template has a PHP error.
            // File is written in cache directory but rendering sent an error in the View::make() method
            if (preg_match("#boubou\/core\/View\.php\([0-9]+\) : eval\(\)'d code#", $moreVars[0][1])) {
                $moreVars[0][0] .= " in <code>" . $debug->file . "</code>";
                $debug->file = View::$last_file_written_in_cache;
                $debug->line = $moreVars[0][2];
                $var = $moreVars[0][0];
                $moreVars = [[]];

                // Do not render error in production.
                if (env('APP_ENV') == 'production') {
                    Log::error('Oups! Error rendering a view', [$debug]);
                    abort(500, 'Oups!');
                }
            }
        }

        $s =  '<div><pre style="' . self::$styles . '">';
        $s .= '<div><small style="color:#ccc">' . $debug->file . (! is_null($debug->line) ? ':' . $debug->line : '') . '</small></div>' . PHP_EOL;
        $s .= self::render($var);
        foreach ($moreVars[0] as $v) {
            if (! $v) {
                switch (gettype($v)) {
                    case 'integer':
                        $v = 0;
                        break;
                    case 'string':
                        $v = '';
                        break;
                    default:
                        $v = gettype($v);
                        break;
                }
            }
            $s .= self::render($v);
        }
        $s .= '</pre></div>';

        if (php_sapi_name() == "cli") {
            $s = strip_tags($s);
        }

        return $s;
    }

    /**
     * Get the file name and line to help debug.
     *
     * @param array $debug debug_backtrace()
     *
     * @return object
     */
    private static function file($debug)
    {
        $i = 0;
        while (
            (basename($debug[$i]['file']) == 'helpers.php'
                || basename($debug[$i]['file']) == 'error.php')
            && isset($debug[$i + 1]['file'])) {
            $i++;
        }
        if (! isset($debug[$i])) {
            $debug[$i] = $debug[0];
        }

        $file = $debug[$i]['file'];
        $line = $debug[$i]['line'];

        // Error in a view?
        if (preg_match('#eval\(\)#', $file)) {
            foreach ($debug as $k => $v) {
                if ($v['function'] == 'view') {
                    $file = realpath(View::path($v['args'][0]));
                    $line = null;
                }
            }
        }

        $file = preg_replace('#' . realpath(__DIR__ . '/..') . '#', '', $file);

        return (object) [
            'file' => $file,
            'line' => $line,
        ];
    }
}
