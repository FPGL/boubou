<?php

namespace Boubou\Core;

use Closure;
use Boubou\Core\Middleware;
use Boubou\Core\Request;

/**
 * The treatments to apply before running the controller sent from the route.
 * Middlewares must be stored in app/Middleware.
 */
trait Middleware
{
    /**
     * @var array
     */
    public $list = [];

    /**
     * @var string
     */
    public static $method = null;

    /**
     * @var object Additional arguments to send to Middleware.
     */
    public static $args = null;

    /**
     * Add middleware for a controller.
     *
     * @example `$this->middleware(MyMiddleware::class [, $options]);`
     *
     * @param Request $request
     * @param object $class
     *
     * @return void
     */
    public function middleware($class, $options)
    {
        $this->list[] = [$class, $options];
    }

    /**
     * Register the final controller to execute then call its middlewares.
     *
     * @param Request $request
     * @param object $class
     * @param string $method
     * @param object $args
     *
     * @return void
     */
    public static function run(Request $request, $class, $method, $args)
    {
        self::$method = $method;
        self::$args = $args;
        self::call($request, $class);
    }

    /**
     * Call next middleware until final().
     * Called as `return $next($request);` in every middleware handler.
     *
     * @param Request $request
     *
     * @return void
     */
    public function next(Request $request)
    {
        $this->call($request, $this);
    }

    /**
     * Call all the middleware until final().
     *
     * @param Request $request
     * @param mixed $class
     *
     * @return void
     */
    private static function call(Request $request, $class)
    {
        if (count($class->list)) {
            $first = new $class->list[0][0];
            $options = $class->list[0][1];
            array_shift($class->list);

            $callback = [$first, 'handle'];
            $closure = function (Request $request) use ($class) {
                call_user_func_array([$class, 'next'], [$request]);
            };
            if (method_exists($first, 'handle')) {
                call_user_func_array($callback, [$request, $closure, $options]);
            } else {
                abort(500, 'Method not found!');
            }
        } else {
            self::final($request, $class);
        }
    }

    /**
     * Final call, the controller initially sent by the route.
     *
     * @param Request $request
     * @param object $class
     *
     * @return void
     */
    private static function final(Request $request, $class)
    {
        $new_class = Controller::class;
        call_user_func_array([$new_class, Middleware::$method], array_merge([$class, $request], Middleware::$args));
    }
}
