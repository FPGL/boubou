<?php

namespace Boubou\Core;

/**
 * Environment variables (in the .env file).
 */
class Env
{
    /**
     * @var string Path to the environment file.
     */
    private static $path = __DIR__ . '/../.env';

    /**
     * Parse environment file.
     *
     * @return void
     */
    public static function init()
    {
        $GLOBALS['env'] = parse_ini_file(self::$path, true);
    }

    /**
     * Get environment variable.
     *
     * @param string $key ie.: DB_CONNECTION, DB_*
     * @param string $default Default value to return
     *
     * @return mixed
     */
    public static function get($key = '', $default = null)
    {
        if (empty($key)) {
            return $GLOBALS['env'];
        }
        if (isset($GLOBALS['env'][$key])) {
            return $GLOBALS['env'][$key];
        }
        if (preg_match('/\*/', $key)) {
            // env('DB_*');
            $a = [];
            foreach ($GLOBALS['env'] as $k => $v) {
                if (preg_match('/' . $key . '/', $k)) {
                    $a[$k] = $GLOBALS['env'][$k];
                }
            }
            return $a;
        }

        return $default;
    }
}
