<?php

namespace Boubou\Core;

use Boubou\Core\Csrf;
use Boubou\Core\Middleware;
use Boubou\Core\Request;
use Closure;

/**
 * Manage HTTP routes (@see routes.php).
 */
class Route
{
    /**
     * @var string get, post, patch, ...
     */
    public static $method = null;

    /**
     * @var Request $_GET, $_POST.
     */
    public static $request = null;

    /**
     * @var string REQUEST_URI.
     */
    public static $uri = null;

    /**
     * @var string
     */
    public static $control = null;

    /**
     * @var array
     */
    public static $args = null;

    /**
     * @var bool
     */
    protected static $exact_match = false;

    /**
     * @var bool Authorize where() method.
     */
    protected static $with_where = false;

    /**
     * @var array Known and authorized verb methods.
     */
    private static $knownmethods = ['delete', 'get', 'patch', 'post', 'put'];

    /**
     * @array
     */
    private static $prefix = [];

    /**
     * Boot
     *
     * @return void
     */
    public static function boot()
    {
        // Secure CSRF.
        // Methods GET, POST or custom $_POST['_method']
        $method = $_SERVER['REQUEST_METHOD'];
        if (isset($_POST['_method'])) {
            $method = $_POST['_method'];
        }

        $method = strtolower($method);
        self::$method = $method;

        $request = null;
        switch (self::$method) {
            case 'patch':
                $request = $_POST;
                break;
            case 'post':
                $request = $_POST;
                break;
            case 'put':
                $request = $_POST;
                break;
            default:
                $request = $_GET;
                break;
        }

        // Inputs file
        $files = null;
        if (count($_FILES)) {
            $files = $_FILES;
        }

        $request = new Request($request, $files);
        self::$request = $request;

        // CSRF
        if (in_array(self::$method, ['patch', 'post', 'put'])) {
            Csrf::isValidOrFail($request);
        }

        // $uri = $_SERVER['REDIRECT_URL'] ?? $_SERVER['REQUEST_URI'];
        $uri = $_SERVER['REQUEST_URI'];
        $uri = preg_replace('#(\?.*)#', '', $uri); // No query string
        self::$uri = $uri;
    }

    /**
     * Run the controller/method found.
     *
     * @return void
     */
    public static function run()
    {
        if (self::$control) {
            $control = explode('@', self::$control);
            $class = '\App\Controllers\\' . $control[0];
            $class = new $class();
            $method = $control[1];

            self::$request->class = $class;
            self::$request->method = $method;

            if (method_exists($class, $method)) {
                Middleware::run(self::$request, $class, $method, self::$args);
            } else {
                abort(500, 'Method not found!');
            }
        } else {
            abort(404, 'Page not found!');
        }
    }

    /**
     * Call a verb contained in self::$knownmethods
     *
     * @param string $uri The route ie.: "/path"
     * @param string $control MyController@myMethod
     * @param string $method get, post, ...
     *
     * @return self
     */
    private static function call($uri, $control, $method)
    {
        self::$with_where = false;

        if (self::$method != $method) {
            return (new self);
        }

        $uri = trim($uri, '/');
        if (count(self::$prefix)) {
            $uri = implode('/', self::$prefix) . $uri;
        } else {
            $uri = '/' . $uri;
        }
        if ($uri != '/') {
            $uri = rtrim($uri, '/');
        }
        $uri = preg_replace('#[/]+#', '/', $uri);

        $pattern = preg_replace('#\{.*?\}#', '([a-z0-9-_]+)', $uri);
        if (! preg_match('#^' . $pattern . '$#', self::$uri, $regs)) {
            return (new self);
        }

        preg_match_all('#\{(.*?)\}#', $uri, $regs2);
        foreach ($regs2[1] as $k => $v) {
            self::$request->$v = $regs[$k + 1];
        }

        if (self::$exact_match) {
            return (new self);
        }
        self::$exact_match = self::$uri === $pattern;

        array_shift($regs);

        self::$with_where = true;
        self::$control = $control;
        self::$args = $regs;

        return (new self);
    }

    /**
     * TODO: Constrain a {pattern} in the path or skip this route
     */
    // public function where($param, $pattern)
    // {
    //     // if (self::$with_where) {
    //     //     echo PHP_EOL . var_export(func_get_args(), 1);
    //     //     echo PHP_EOL . self::$uri;
    //     //     echo PHP_EOL . '---------------------------------------';
    //     // }
    //
    //     return (new self);
    // }

    /**
     * Add a prefix to the route URI.
     *
     * @param string $prefix Uri
     * @param Closure $next
     *
     * @return self;
     */
    public static function prefix($prefix, Closure $next)
    {
        $prefix = '/' . trim($prefix, '/') . '/';
        self::$prefix[] = $prefix;
        $next();
        array_pop(self::$prefix);

        return (new self);
    }

    /**
     * TODO: group @see prefix()
     */
    // public function group($param, Closure $next)
    // {
    //     return (new self);
    // }

    /**
     * Catch undeclared methods
     *
     * @param string $method
     * @param mixed $arguments
     *
     * @return mixed
     */
    public static function __callStatic($method, $arguments)
    {
        if (in_array($method, self::$knownmethods)) {
            // dump($method, $arguments);
            return self::call($arguments[0], $arguments[1], $method);
        }
    }
}
