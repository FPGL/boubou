<?php

namespace Boubou\Core;

/**
 * Writes a new log line (info, debug, error, ...).
 */
class Log
{
    /**
     * @var string Log file.
     */
    private static $output_file = __DIR__ . '/./../storage/logs/boubou.log';

    protected static $levels = [
        'error',
        'warning',
        'notice',
        'info',
        'debug',
    ];

    /**
     * Write to the log output.
     *
     * @param string $level
     * @param array $arguments
     */
    public static function log($level, $arguments)
    {
        $message = $arguments[0];

        $context = '';
        if (isset($arguments[1])) {
            $context = $arguments[1];
            if (! is_array($context)) {
                throw new CustomException('Log context must be an array! (' . gettype($context) . ')');
            }
            $context = json_encode($context);
        }

        $date = date('Y-m-d H:i:s');
        $ip = request()->getClientIp();

        $message = '[' . $date . '][' . $ip . '] '
            . env('APP_ENV') . '.' . strtoupper($level)
            . ' ' . $message
            . ($context ? ' ' . $context : '');

        if ($fp = fopen(self::$output_file, 'a+')) {
            fwrite($fp, $message . PHP_EOL);
            fclose($fp);
        }
    }

    /**
     * Call static method.
     *
     * @param string $method
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic($method, $arguments)
    {
        if (in_array($method, self::$levels)) {
            self::log($method, $arguments);
        } else {
            throw new CustomException('Method "' . $method . '" not allowed!');
        }
    }
}
