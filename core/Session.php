<?php

namespace Boubou\Core;

/**
 * Session element.
 */
class Session
{
    /**
     * Configure session.
     */
    public function config()
    {
        session_set_cookie_params(
            config('session')['lifetime'],
            config('session')['path'],
            config('session')['domain'],
            config('session')['secure'],
            config('session')['httponly']
        );
        session_name(config('session')['name']);
    }

    /**
     * Initialize the Session.
     *
     * @return void
     */
    public function start()
    {
        $this->config();
        session_start();
    }

    /**
     * Destroying session.
     */
    public static function destroy()
    {
        session_destroy();
    }

    /**
     * Return the session.
     *
     * @return array
     */
    public function all()
    {
        return $_SESSION;
    }

    /**
     * Add or replace a session value.
     *
     * @param string $name
     * @param mixed $value
     *
     * @return void
     */
    public function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    /**
     * Remove a session value.
     *
     * @param string $name
     *
     * @return void
     */
    public function unset($name)
    {
        if ($this->has($name)) {
            unset($_SESSION[$name]);
        }
    }

    /**
     * Get a session value.
     *
     * @param string $name
     *
     * @return mixed|void
     */
    public function get($name)
    {
        if ($this->has($name)) {
            return $_SESSION[$name];
        }
    }

    /**
     * Session has property.
     *
     * @param string $name
     *
     * @return bool
     */
    public function has($name)
    {
        return isset($_SESSION[$name]);
    }
}
