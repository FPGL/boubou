<?php

function log_error($errno, $errstr, $errfile, $errline)
{
    dde($errno, $errstr, $errfile, $errline);
}

function log_exception($e)
{
    dde("<h1 style='line-height: 24px;'>" . trim(preg_replace("#[ ]+#", " ", $e->getMessage())) . "</h1>", $e);
}

set_error_handler('log_error');
set_exception_handler('log_exception');

if (env('APP_ENV') == 'production') {
    error_reporting(0);
    ini_set('display_errors', 0);
} else {
    error_reporting(E_ALL | E_STRICT);
    ini_set('display_errors', 1);
}
