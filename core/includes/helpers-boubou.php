<?php

/*
Because there is no namespace on this file.
It could be usefull to use:
if (! function_exists('myfunc')) {
    function myfunc() {}
}
*/

/**
 * CLI Help.
 *
 * @return string
 */
function help()
{
    return "boubou help -- Print this help content" . PHP_EOL
        . "boubou migrate -- Re-run all mirations" . PHP_EOL
        . "boubou migrate:up -- Up all mirations" . PHP_EOL
        . "boubou migrate:down -- Down all mirations" . PHP_EOL
        . "boubou migrate:create [create_examples_table] -- Create a new migration (plural recommended)" . PHP_EOL
        . "boubou controller:create [Controller Name] -- Create a new controller (ie.: \"ExamplesController\")" . PHP_EOL
        . "boubou model:create [Model Name] -- Create a new model (singular recommended. ie.: \"Example\")" . PHP_EOL
        . "boubou publish -- Copy css, js and images files to public/" . PHP_EOL
        . "boubou cache:clear -- Clear cache and logs" . PHP_EOL
        . "boubou down -- Website is down" . PHP_EOL
        . "boubou up -- Website is up";
}

/**
 * Copy directory recursively.
 *
 * @param string $source Source dir.
 * @param string $destination Destination dir.
 *
 * @return string A message.
 */
function copy_directory($source, $destination)
{
    $dir = opendir($source);
    if (! is_dir($destination)) {
        mkdir($destination);
    }
    while (false !== ($file = readdir($dir))) {
        if ($file != '.' && $file != '..') {
            if (is_dir($source . '/' . $file)) {
                recurse_copy($source . '/' . $file, $destination . '/' . $file);
            } else {
                copy($source . '/' . $file, $destination . '/' . $file);
            }
        }
    }
    closedir($dir);
}

/**
 * Copy assets dir to public dir.
 *
 * @param array $argv Arguments received from CLI.
 *
 * @return string A message.
 */
function publish($argv)
{
    $basedir = realpath(__DIR__ . '/./../../resources/');
    $destdir = realpath(__DIR__ . '/./../../public/');
    $authorized_dirs = ['css', 'images', 'js'];

    array_shift($argv);
    array_shift($argv);

    if (! count($argv)) {
        $argv = $authorized_dirs;
    }

    foreach ($argv  as $dir) {
        if (in_array($dir, $authorized_dirs)) {
            $from = $basedir . '/' . $dir;
            $to = $destdir . '/' . $dir;
            if (! is_dir($from)) {
                return 'Oups! "' . $from . '" is not a valid source directory.';
            }
            if (! is_dir($to)) {
                return 'Oups! "' . $to . '" is not a valid destination directory.' . PHP_EOL
                    . "Do a `mkdir -p $to`";
            }
            copy_directory($from, $to);
        }
    }

    return 'Copied directories: "' . implode('", "', $argv) . '".';
}

/**
 * Clear cached files et empty logs files.
 *
 * @return string A message.
 */
function clearCacheAndLogs()
{
    $message = '';
    $storage_dir = realpath(__DIR__ . '/./../../storage/');

    $cache_dir = $storage_dir . '/cache/';
    $dir = opendir($cache_dir);
    $i = 0;
    while (false !== ($file = readdir($dir))) {
        if ($file != '.' && $file != '..' && $file != '.gitignore') {
            if (is_file($cache_dir . '/' . $file)) {
                unlink($cache_dir . '/' . $file);
                $i++;
            }
        }
    }
    closedir($dir);

    $message .= $i . " files removed from cache." . PHP_EOL;

    $logs_dir = $storage_dir . '/logs/';
    $dir = opendir($logs_dir);
    while (false !== ($file = readdir($dir))) {
        if ($file != '.' && $file != '..' && $file != '.gitignore') {
            if (is_file($logs_dir . '/' . $file)) {
                $handle = fopen($logs_dir . '/' . $file, 'r+');
                $linecount = 0;
                while (! feof($handle)) {
                    $line = fgets($handle, 4096);
                    $linecount = $linecount + substr_count($line, PHP_EOL);
                }
                ftruncate($handle, 0);
                fclose($handle);
                $message .= "$file is empty: $linecount line(s) removed." . PHP_EOL;
            }
        }
    }
    closedir($dir);

    return $message;
}

function boubouIsDown()
{
    return 'Website is down!';
}

function boubouIsUp()
{
    return 'Website is up!';
}
