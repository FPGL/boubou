<?php

use Boubou\Core\Database\Migration;
use Boubou\Core\Dumper;
use Boubou\Core\Env;
use Boubou\Core\Log;
use Boubou\Core\Request;
use Boubou\Core\Response;
use Boubou\Core\Session;
use Boubou\Core\View;

/*
Because there is no namespace on this file.
It could be usefull to use:
if (! function_exists('myfunc')) {
    function myfunc() {}
}
*/

/**
 * Dump.
 * @example `dump(99, -98.0, 'toot', 12.343, " ", ['a', 'b', -4.01]);`
 *
 * @param mixed $var
 * @param mixed $moreVars
 *
 * @return void
 */
function dump($var, ...$moreVars)
{
    Dumper::dump($var, $moreVars);
}

/**
 * Dump and die.
 * dd(99, -98.0, 'toot', 12.343, " ", ['a', 'b', -4.01]);
 *
 * @param mixed $var
 * @param mixed $moreVars
 *
 * @return void
 */
function dd($var, ...$moreVars)
{
    Dumper::dd($var, $moreVars);
}

/**
 * Dump and die errors and exceptions.
 *
 * @param mixed $var
 * @param mixed $moreVars
 *
 * @return void
 */
function dde($var, ...$moreVars)
{
    if (ob_get_contents()) {
        ob_end_clean();
    }

    // Log::error('Errors Exceptions', [$var, $moreVars]);

    Dumper::isError()->allowHtml(true)->dd($var, $moreVars);
}

/**
 * Get environment variables.
 *
 * @param string $key ie.: DB_CONNECTION
 * @param string $default Default value to return
 *
 * @return mixed
 */
function env($key = '', $default = null)
{
    return Env::get($key, $default);
}

/**
 * Get config property.
 *
 * @param string $key
 *
 * @return mixed
 */
function config($key = null)
{
    $config = require __DIR__ . '/../../config.php';

    if (! empty($key)) {
        return $config[$key];
    }

    return $config;
}

/**
 * Execute migrations.
 *
 * @return void
 */
function migrate()
{
    return Migration::run();
}

/**
 * View helpers.
 *
 * @param string $path
 * @param array $data
 *
 * @return void
 */
function view($path = '', $data = [], $entities = false)
{
    $res = View::make($path, $data, $entities);
}

/**
 * Redirect response.
 *
 * @param string $location Location uri
 * @param int $code Option code header
 *
 * @return void
 */
function redirect($location = '/', $code = null)
{
    Response::redirect($location, $code);
}

/**
 * Abort response.
 *
 * @param int $code 404, 500, ...
 * @param string $message Optionnal message
 *
 * @return void
 */
function abort($code, $message = '')
{
    Response::abort($code, $message);
}

/**
 * Response helper.
 *.
 * @return Response
 */
function response()
{
    return (new Response);
}

/**
 * Return JSON data.
 *
 * @param mixed $data
 *
 * @return string
 */
function json($data)
{
    return response()->json($data);
}

/**
 * Redirect to previous url.
 *
 * @return void
 */
function back()
{
    Response::back();
}

/**
 * Request helper.
 *
 * @return Request
 */
function request()
{
    return (new Request($_GET));
}

/**
 * Crypt password.
 *
 * @param string $value
 * @param string $options
 *
 * @return string
 */
function bcrypt($value, $options = ['cost' => 10])
{
    return password_hash($value, PASSWORD_BCRYPT, $options);
}

/**
 * Obfuscate a string.
 * @example obfuscate('mailto:toto@toto.com')
 *
 * @param string $string
 *
 * @return string
 */
function obfuscate($string)
{
    $s = '';
    foreach (str_split($string) as $v) {
        switch (rand(1, 4)) {
            case 1:
                $s .= '&#' . ord($v) . ';';
                break;
            case 2:
                $s .= '&#x' . dechex(ord($v)) . ';';
                break;
            default:
                $s .= $v;
        }
    }

    return $s;
}

/**
 * Session helper.
 *.
 * @return Response
 */
function session()
{
    return (new Session);
}

/**
 * String to slug.
 *
 * @param string $string
 *
 * @return string
 */
function slug($string)
{
    $slug = str_replace(" ", "-", $string);
    $slug = preg_replace('/[^\w\d\-\_]/i', '', $slug);
    $slug = preg_replace('/[-]{2,}/', '-', $slug);
    $slug = preg_replace('/^[-]+|[-]+$/', '', $slug);

    return strtolower($slug);
}
