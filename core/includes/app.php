<?php

include __DIR__ . '/../../core/includes/helpers.php'; // Helpers.
include __DIR__ . '/../../core/includes/error.php'; // Error handler.

// Profiling with xhprof.
$xhprof_enabled = env('APP_ENV', false) == 'local';
if ($xhprof_enabled && function_exists('tideways_xhprof_enable')) {
    tideways_xhprof_enable();
}

use Boubou\Core\Env;
use Boubou\Core\Route;

// Session
session()->start();

// Load .env variables.
Env::init();

// Catch the routes.
Route::boot();
require __DIR__ . '/../../routes.php';
Route::run();

// At the end... save the profiling record.
if ($xhprof_enabled && function_exists('tideways_xhprof_disable')) {
    $data = tideways_xhprof_disable();
    // dd($data); // http://www.kam24.ru/xhprof/docs/index.html
    // sys_get_temp_dir()
    file_put_contents(
        realpath(__DIR__ . '/../../storage'). '/xhprof/' . uniqid() . '.boubou.xhprof',
        serialize($data)
    );
}
