<?php
/**
 * Register the autoloader.
 */
spl_autoload_register(function ($class) {
    $class = ltrim($class, '\\');
    $file = '';
    if ($lastNsPos = strripos($class, '\\')) {
        $namespace = substr($class, 0, $lastNsPos);
        $class = substr($class, $lastNsPos + 1);
        $file = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }

    $paths = [
        '#^(/?)?Boubou/#' => '/',
        '#^(/?)?App/#' => '../../app/',
        '#^(/?)?Core/#' => '../../core/'
    ];
    $file = preg_replace(array_keys($paths), array_values($paths), $file);

    $file = __DIR__ . DIRECTORY_SEPARATOR . $file . $class . '.php';

    if (file_exists($file)) {
        require $file;

        return true;
    }

    return false;
});
