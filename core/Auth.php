<?php

namespace Boubou\Core;

use App\User;

/**
 * Basic authentication methods.
 */
class Auth
{
    /**
     * Return user
     *
     * @return User
     */
    private function user()
    {
        static $user = null;

        if ($user) {
            return $user;
        }

        $user_id = session()->get('user_id');

        if ($user_id) {
            $user = User::where('id', '=', $user_id)->first();

            if (! $user->id) {
                redirect('/logout#unf');
            }
        }

        return $user;
    }

    /**
     * Call static method.
     *
     * @param string $method
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic($method, $arguments)
    {
        return call_user_func_array([new static, $method], $arguments);
    }
}
