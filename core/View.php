<?php

namespace Boubou\Core;

use Boubou\Core\CustomException;
use Boubou\Core\Response;
use Boubou\Core\Template;

/**
 * Manage templates views stored in resources/views/.
 */
class View
{
    /**
     * @var string Views directory.
     */
    private static $basedir = __DIR__ . '/./../resources/views/';

    /**
     * @var array
     */
    private static $error = [];

    /**
     *
     */
    private static $error_code = null;

    /**
     * Last file written in cache
     */
    public static $last_file_written_in_cache = '';

    /**
     * Make the view.
     *
     * @param string $path
     * @param array $data
     * @param bool $entities If "false apply htmlentities
     *
     * @return void|Response
     *
     * @throws CustomException If failed to compiled.
     */
    public static function make($path = '', $data = [], $entities = false)
    {
        if (count(self::$error)) {
            return false;
        }

        $php = self::template($path);
        $template = Template::build($php);

        $php = "<?php" . PHP_EOL;

        // if (env('APP_ENV') == 'local') {
        //     $php .= '$time = microtime(TRUE); $mem = memory_get_usage();' . PHP_EOL;
        // }

        // Available class in template.
        $php .= 'use Boubou\Core\Auth;' . PHP_EOL;
        $php .= 'use Boubou\Core\Csrf;' . PHP_EOL;
        $php .= 'use Boubou\Core\File;' . PHP_EOL;
        $php .= 'use Boubou\Core\Request;' . PHP_EOL;

        $php .= self::vars($data, $entities);
        $php .= "?>";
        $php .= $template;

        /*
        if (env('APP_ENV') == 'local') {
            // Send this array to server logs.
            $php .= "<?php \Boubou\Core\Log::info('memory: ' . (memory_get_usage() - \$mem) / (1024 * 1024)); ?>" . PHP_EOL;
            $php .= "<?php \Boubou\Core\Log::info('seconds: ' . (microtime(TRUE) - \$time)); ?>" . PHP_EOL;
        }
        */

        // TODO: Use the cache
        self::put($path, $php);

        ob_start();
        eval(' ?>' . $php);
        $html = ob_get_contents();
        ob_end_clean();


        if (php_sapi_name() == "cli") {
            $html = preg_replace('#<head>.*</head>#msiU', '', $html);
            $html = preg_replace('#[\s]{2}#msiU', '', $html);
            $html = strip_tags($html);
            $html = trim($html) . PHP_EOL;
        }

        self::$error = error_get_last();

        if (self::$error) {
            throw new CustomException(self::$error['message']);
        } else {
            return Response::view($html);
        }
    }

    /**
     * Create PHP vars from initial "compact" argument.
     *
     * @param array $data
     * @param bool $entities If "false apply htmlentities
     *
     * @return string $php
     */
    private static function vars($data, $entities)
    {
        $php = '';
        foreach ($data as $k => $v) {
            $type = strtolower(gettype($v));
            switch ($type) {
                case 'array':
                    $v = var_export($v, 1);
                    $php .= "\$$k = (array) $v;" . PHP_EOL;
                    break;
                case 'double':
                    $php .= "\$$k = (double) $v;" . PHP_EOL;
                    break;
                case 'integer':
                    $php .= "\$$k = (int) $v;" . PHP_EOL;
                    break;
                case 'object':
                    if (get_class($v) != 'stdClass' && ! method_exists($v, '__set_state')) {
                        throw new CustomException(get_class($v) . ' must implement __set_state() to keep its methods with var_export().');
                    }
                    $v = var_export($v, 1);
                    $php .= "\$$k = (object) $v;" . PHP_EOL;
                    break;
                case 'string':
                    if (! $entities) {
                        $v = htmlentities($v);
                    }
                    $php .= "\$$k = (string) \"" . $v . "\";" . PHP_EOL;
                    break;
                case 'null':
                    $php .= "\$$k = null;" . PHP_EOL;
                    break;
                default:
                    var_export($type);
                    var_export($v);
                    exit(__METHOD__);
                    break;
            }
        }

        $php = preg_replace_callback(
            '#([a-zA-Z0-9\\\]+)::([a-zA-Z0-9_]+)#',
            function ($matches) {
                $obj = $matches[1];
                $method = $matches[2];

                return method_exists($obj, $method)
                    ? $obj . '::' . $method
                    : '';
            },
            $php
        );

        return $php;
    }

    /**
     * Path to the view.
     *
     * @param string $path
     *
     * @return string
     */
    public static function path($path)
    {
        $path = preg_replace('#\.#', '/', $path);
        $path = self::$basedir . $path . '.blade.php';
        $path = realpath($path);

        return $path;
    }

    /**
     * Get the source template.
     *
     * @param string $path
     *
     * @return string
     */
    private static function template($path)
    {
        $path = self::path($path);
        $content = file_get_contents($path);
        $content = Template::stripComments($content);

        return $content;
    }

    /**
     * Include a view into another view.
     *
     * @param string $path
     *
     * @return void
     */
    private static function include($path)
    {
        return (include self::path($path));
    }

    /**
     * Do this view exists?
     *
     * @param string $path
     *
     * @param bool
     */
    public static function exists($path)
    {
        $path = self::path($path);

        return is_file($path);
    }

    /**
     * Write the generated php code.
     *
     * @param string $path
     * @param string $php
     *
     * @return void
     */
    private static function put($path, $php)
    {
        $path = self::path($path);
        $basedir = self::path(self::$basedir);
        $path = preg_replace('#' . $basedir . '#', '', $path);
        $path = md5($path) . '.php';
        $cache = realpath(__DIR__ . '/../storage/cache');
        $path = $cache . '/' . $path;

        self::$last_file_written_in_cache = $path;

        file_put_contents($path, $php);
    }

    /**
     * Get the errors bag.
     *
     * @return array
     */
    public static function getError()
    {
        return self::$error;
    }
}
