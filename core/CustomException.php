<?php

namespace Boubou\Core;

/**
 * Manage a custom exception.
 */
class CustomException extends \Exception
{
    /**
     * @var array
     */
    private $params;

    /**
     * @param string $message
     * @param int $code
     * @param Throwable $previous
     */
    public function __construct($message, $code = 0, Exception $previous = null, $params = [])
    {
        parent::__construct($message, $code, $previous);

        $this->setParams($params);
    }

    /**
     * Set exception param.
     *
     * @param array $params
     *
     * @return $this
     */
    public function setParams(array $params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get exception param.
     *
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Print the error.
     *
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
