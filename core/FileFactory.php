<?php

namespace Boubou\Core;

/**
 * Generate template files for boubou (migration, controller, model).
 */
class FileFactory
{
    /**
     * @var array
     */
    private static $paths = [
        'controllers' => __DIR__ . '/../app/Controllers',
        'migrations' => __DIR__ . '/../database',
        'models' => __DIR__ . '/../app',
    ];

    /**
     * @var string
     */
    private static $tpl_migration = '<?php

use Boubou\Core\Database\Migration;
use Boubou\Core\Database\Table;
use Boubou\Core\Database\Schema;

/**
 * {{ $name }} migration.
 */
class {{ $class_name }} extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {{ $tpl_create }}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        {{ $tpl_drop }}
    }
}
';

    /**
     * @var string
     */
    private static $tpl_controller = '<?php

namespace App\Controllers;

use Boubou\Core\Controller;
use Boubou\Core\Request;

/**
 * {{ $name }} controller.
 */
class {{ $class_name }} extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }
}
';

    private static $tpl_model = '<?php

namespace App;

use Boubou\Core\Model;

/**
 * {{ $name }} model.
 */
class {{ $class_name }} extends Model
{
    //
}
';

    /**
     * Create a new migration.
     *
     * @param string $name
     *
     * @return string A message.
     */
    public static function createMigration($name)
    {
        $class_name = self::getClassName($name);

        $last_index = 00000;

        $migrations = glob(self::$paths['migrations'] . '/*');
        foreach ($migrations as $k => $migration) {
            preg_match('#([0-9]+)_(.*)#', $migration, $regs);
            $last_index = max($last_index, $regs[1]);
        }

        $last_index++;
        $last_index = str_pad($last_index, 4, '0', STR_PAD_LEFT);

        $date = date('Ymd');

        $filename = realpath(self::$paths['migrations']) . '/' . $last_index . '_' . $date . '_' . $name . '.php';

        $tpl = self::$tpl_migration;
        $tpl_create = 'Schema::create(\'{{ $name }}\', function (Table $table) {
                $table->primary(\'id\');
                $table->timestamps();
            });';
        $tpl_drop = 'Schema::drop(\'{{ $name }}\');';

        if (preg_match('#^create_#', $name)) {
            $tpl = preg_replace('#' . preg_quote('{{ $tpl_create }}') . '#', $tpl_create, $tpl);
            $tpl = preg_replace('#' . preg_quote('{{ $tpl_drop }}') . '#', $tpl_drop, $tpl);
        }
        $tpl = preg_replace('#' . preg_quote('{{ $tpl_create }}') . '#', '//', $tpl);
        $tpl = preg_replace('#' . preg_quote('{{ $tpl_drop }}') . '#', '//', $tpl);

        $name = preg_replace(['#^create_#', '#_table$#',], ['', ''], $name);

        $tpl = preg_replace('#' . preg_quote('{{ $name }}') . '#', $name, $tpl);
        $tpl = preg_replace('#' . preg_quote('{{ $class_name }}') . '#', $class_name, $tpl);

        file_put_contents($filename, $tpl);

        return 'New migration created in ' . preg_replace('#' . preg_quote(realpath('.') . '/') . '#', '', $filename) . '.';
    }

    /**
     * Create a new controller.
     *
     * @param string $name
     *
     * @return string A message.
     */
    public static function createController($name)
    {
        $class_name = self::getClassName($name);

        $filename = realpath(self::$paths['controllers']) . '/' . $name . '.php';

        if (is_file($filename) || is_dir($filename)) {
            return 'Controller already exists: ' . $filename;
        }

        $tpl = self::$tpl_controller;

        $tpl = preg_replace('#' . preg_quote('{{ $name }}') . '#', $name, $tpl);
        $tpl = preg_replace('#' . preg_quote('{{ $class_name }}') . '#', $class_name, $tpl);

        file_put_contents($filename, $tpl);

        return 'New controller created in ' . preg_replace('#' . preg_quote(realpath('.') . '/') . '#', '', $filename) . '.';
    }

    /**
     * Create a new model.
     *
     * @param string $name
     *
     * @return string A message.
     */
    public static function createModel($name)
    {
        $class_name = self::getClassName($name);

        $filename = realpath(self::$paths['models']) . '/' . $name . '.php';

        if (is_file($filename) || is_dir($filename)) {
            return 'Model already exists: ' . $filename;
        }

        $tpl = self::$tpl_model;

        $tpl = preg_replace('#' . preg_quote('{{ $name }}') . '#', $name, $tpl);
        $tpl = preg_replace('#' . preg_quote('{{ $class_name }}') . '#', $class_name, $tpl);

        file_put_contents($filename, $tpl);

        return 'New model created in ' . preg_replace('#' . preg_quote(realpath('.') . '/') . '#', '', $filename) . '.';
    }

    /**
     * Transform the name of the class sent.
     *
     * @param string $name
     *
     * @return $name
     */
    private static function getClassName($name)
    {
        $class_name = preg_replace('#_#', " ", $name);
        $class_name = ucwords($class_name);
        $class_name = preg_replace('# #', "", $class_name);

        return $class_name;
    }
}
