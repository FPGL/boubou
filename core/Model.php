<?php

namespace Boubou\Core;

use Boubou\Core\Database\Orm;
use Boubou\Core\Database\Relationship;

/**
 * Main Model class.
 * Models must be stored in app/.
 */
class Model
{
    use Orm;
    use Relationship;

    /**
     * @var string The table associated with the model.
     */
    protected $table;

    /**
     * @var array
     */
    public $attributes = [];

    /**
     * Guess the table name if $table is null.
     *
     * @return string
     */
    public function getTable()
    {
        $model = (new static);

        if (! $model->table) {
            $short_name = (new \ReflectionClass($model))->getShortName();
            $short_name = strtolower($short_name);
            if (! preg_match('/s$/', $short_name)) {
                $short_name .= 's';
            }
            $model->table = $short_name;
        }

        return $model->table;
    }

    /**
     * Get fillable columns for this model.
     *
     * @return array
     */
    private function getFillable()
    {
        $model = (new static);

        return $model->fillable;
    }

    /**
     * Fill the model with the following data.
     *
     * @param array $data
     *
     * @return void
     */
    public function fill($data = [])
    {
        foreach ($data as $k => $v) {
            if (gettype($data) == 'object') {
                $this->set($k, $v);
            } else {
                $this->$k = $v;
            }
        }
    }

    /**
     * Return the request as array.
     *
     * @return array
     */
    public function toArray()
    {
        $res = (array) $this->attributes;

        // Add eager loaded sub-models to array
        foreach ($this->relations2 as $name => $model) {
            $res[$name] = $model->toArray();
        }

        return $res;
    }

    /**
     * Shorter to Database\Result::has().
     *
     * @param string $value
     * @param string $column
     *
     * @return bool
     */
    public function has($value, $column = 'id')
    {
        return $this->get()->has($value, $column);
    }

    /**
     * @param mixed $column_key
     * @param mixed $index_key
     */
    public function list($column_key = '', $index_key = null)
    {
        return array_column($this->toArray(), $column_key, $index_key);
    }

    /**
     * Write data to an attribute property.
     *
     * @param string $name
     * @param mixed $value
     *
     * @return void
     */
    public function set($name, $value)
    {
        // $debug = debug_backtrace();
        // dump('set()', $name, $value, $debug);

        $this->attributes[$name] = $value;
    }

    /**
     * Is run when writing data to inaccessible properties.
     *
     * @param string $name
     * @param mixed $value
     *
     * @return void
     */
    public function __set($name, $value)
    {
        $this->set($name, $value);
    }

    /**
     * Is utilized for reading data from inaccessible properties.
     * TODO: Do not create a get() method. Will be confusing with $model->get() from the ORM.
     *
     * @param string $name
     *
     * @return mixed Attribute property, a relation or null.
     */
    public function __get($name)
    {
        if (isset($this->attributes[$name])) {
            return $this->attributes[$name];
        }

        // Look for an "easy loaded" relation.
        if ($relation = $this->getRelation($name)) {
            return $relation;
        }

        // Accessing a property relation. Ie.: $user->roles and User::roles() exists.
        // TODO Manage for $user->roles()->get();
        if ($this->hasRelation($name)) {
            return $this->relation($name);
        }

        return null;
    }

    /**
     * Is invoked when unset() is used on inaccessible properties.
     *
     * @param string $name
     *
     * @return void
     */
    public function __unset($name)
    {
        unset($this->attributes[$name]);
    }

    /**
     * Used by View.php when constructing a model instance.
     * @example $user = (object) App\User::__set_state([properties]);
     *
     * @param array $data
     *
     * @return Model?
     */
    public static function __set_state($data)
    {
        $model = (new static);
        $model->fill($data);

        return $model;
    }

    /**
     * Call static method.
     *
     * @param string $method
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic($method, $arguments)
    {
        return call_user_func_array([new static, $method], $arguments);
    }
}
