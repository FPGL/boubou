<?php

namespace Boubou\Core;

use Boubou\Core\View;

/**
 * An HTTP response: An error code, an html view, or a json string.
 */
class Response
{
    /**
     * Abort response.
     *
     * @param int $code Code error (404, 500, ...).
     * @param string $message Optionnal message.
     *
     * @return void
     */
    public static function abort($code, $message = '')
    {
        self::http_response_code($code);

        $path = 'errors.' . $code;
        if (View::exists($path)) {
            View::make($path, compact('code', 'message'));
        }

        die();
    }

    /**
     * Redirect back.
     *
     * @return void
     */
    public static function back()
    {
        if (! empty($_SERVER['HTTP_REFERER'])) {
            self::redirect($_SERVER['HTTP_REFERER']);
        }
        if (! empty($_SERVER['REDIRECT_URL'])) {
            self::redirect($_SERVER['REDIRECT_URL']);
        }
    }

    /**
     * Used by abort(). Missing http_response_code().
     * For 4.3.0 <= PHP <= 5.4.0
     *
     * @param int $code 404, 500, ...
     */
    public static function http_response_code($newcode = null)
    {
        if (! function_exists('http_response_code')) {
            static $code = 200;
            if ($newcode !== null) {
                header('X-PHP-Response-Code: ' . $newcode, true, $newcode);
                if (! headers_sent()) {
                    $code = $newcode;
                }
            }

            return $code;
        }

        return http_response_code($newcode);
    }

    /**
     * Redirect response.
     *
     * @param string $location Location uri.
     * @param int $code Option code header.
     *
     * @return void
     */
    public static function redirect($location = '/', $code = null)
    {
        if ($code) {
            self::http_response_code($code);
        }

        if (! headers_sent()) {
            header("Location:$location");
            die();
        }

        return self;
    }

    /**
     * Print the rendered view.
     *
     * @param string $html.
     *
     * @return self
     */
    public static function view($html)
    {
        // TODO: With an "echo" we can not apply a middleware after rendering the response :(
        // So, how to look to bactrace and detect last chained method.
        // $tmp = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        // dump($tmp);

        echo $html;

        return (new self);
    }

    /**
     * Create a new JSON response instance.
     *
     * @param mixed $data Data to encode.
     *
     * @return string
     */
    public static function json($data = [])
    {
        return json_encode($data);
    }
}
