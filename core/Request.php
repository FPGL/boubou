<?php

namespace Boubou\Core;

use App\User;
use Boubou\Core\Auth;
use Boubou\Core\File;

/**
 * Request element.
 */
class Request
{
    /**
     * @var array
     */
    public $attributes = [];

    /**
     * @var array
     */
    public $files = [];

    /**
     * @var mixed Controller class requested through a route.
     */
    public $class = null;

    /**
     * @var string Requested method requested through a route.
     */
    public $method = null;

    /**
     * Get the request and assign it to $this object
     *
     * @param array $request
     * @param array|null $files
     */
    public function __construct(array $request = [], $files = [])
    {
        foreach ($request as $k => $v) {
            $this->set($k, $v);
        }

        // Transform a file sent by a form to a File object.
        if ($files && count($files)) {
            foreach ($files as $k => $file) {
                if (is_array($file['name'])) {
                    $this->files[$k] = [];
                    for ($i = 0; $i < count($file['name']); $i++) {
                        $this->files[$k][$i] = [];
                        foreach ($file as $name => $value) {
                            $this->files[$k][$i][$name] = $value[$i];
                        }
                        $this->files[$k][$i] = new File($this->files[$k][$i]);
                    }
                } else {
                    $this->files[$k] = new File($file);
                }
            }
        }
    }

    /**
     * Return the request as array.
     *
     * @return array
     */
    public function all()
    {
        return (array) array_merge($this->attributes, $this->files);
    }

    /**
     * Get session or get/set a session property.
     * TODO: The class Session was written after the class Request, not sure that this method is still a lot of sense.
     *
     * @param string $name
     * @param mixed $value
     *
     * @return array|mixed|null
     */
    public function session($name = '', $value = null)
    {
        if (empty($name)) {
            return session()->all();
        }
        if (! empty($value)) {
            session()->set($name, $value);
        }
        if (session()->has($name)) {
            return session()->get($name);
        }

        return null;
    }

    /**
     * Get the authenticated user.
     *
     * @return User
     */
    public function user()
    {
        return Auth::user();
    }

    /**
     * Get the current url.
     *
     * @return string
     */
    public function url()
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * Check if a pattern exists in the current url.
     * @example request()->match('/admin/posts*')
     *
     * @param string $regex
     *
     * @return bool
     */
    public function match($regex)
    {
        $url = $this->url();
        $regex = preg_replace('/\//', '\/', $regex);
        $regex = preg_replace('/\*/', '(.*?)', $regex);

        return preg_match('/^' . $regex . '$/', $url);
    }

    /**
     * Write data to an "attribute" property.
     *
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * Get data from "attribute" property.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function get($name)
    {
        return $this->attributes[$name] ?? ($this->files[$name] ?? '');
    }

    /**
     * Get client IP
     */
    public function getClientIp()
    {
        $ip = null;

        if (! empty($_SERVER['REMOTE_ADDR'])) {
            return $_SERVER['REMOTE_ADDR'];
        }
        if (empty($ip) && ! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        if (empty($ip) && ! empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }

        return $ip;
    }

    /**
     * Is run when writing data to inaccessible properties.
     *
     * @param string $name
     * @param mixed $value
     *
     * @return void
     */
    public function __set($name, $value)
    {
        $this->set($name, $value);
    }

    /**
     * Is utilized for reading data from inaccessible properties.
     *
     * @param string $name
     *
     * @return mixed Attribute property.
     */
    public function __get($name)
    {
        return $this->attributes[$name] ?? ($this->files[$name] ?? null);
    }

    /**
     * Is invoked when unset() is used on inaccessible properties.
     *
     * @param string $name
     *
     * @return void
     */
    public function __unset($name)
    {
        if (isset($this->attributes[$name])) {
            unset($this->attributes[$name]);
        }
        if (isset($this->files[$name])) {
            unset($this->files[$name]);
        }
    }
}
