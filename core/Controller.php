<?php

namespace Boubou\Core;

/**
 * Main controller class.
 * Controllers must be stored in app/Controllers.
 */
class Controller
{
    use Middleware;

    /**
     * Catch method call in class from the route: ie.: BlogController@index.
     */
    public static function __callStatic($method, $args)
    {
        $class = $args[0];
        array_shift($args);

        return call_user_func_array([$class, $method], $args);
    }
}
