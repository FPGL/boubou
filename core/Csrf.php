<?php

namespace Boubou\Core;

use Boubou\Core\Request;

/**
 * Cross-Site Request Forgery (CSRF).
 */
class Csrf
{
    /**
     * @var int Default token duration (second) (@see config.php).
     */
    private static $max_time = 180; // 180 = 3 minutes.

    /**
     * Generate new token.
     *
     * @return string Token value.
     */
    public static function token()
    {
        $request = request();
        $token = bin2hex(random_bytes(32));
        $request->session('csrf_token', $token);
        $request->session('csrf_token_time', time());

        return $token;
    }

    /**
     * Check if CSRF token is valid or fail.
     *
     * @param Request $request
     *
     * @return void
     */
    public static function isValidOrFail(Request $request)
    {
        $csrf_token = $request->csrf_token;
        $session_csrf_token = $request->session('csrf_token');
        $session_csrf_token_time = $request->session('csrf_token_time');

        if (empty($csrf_token) || empty($session_csrf_token)) {
            abort(403, 'Token is missing!');
        } elseif (! hash_equals($session_csrf_token, $csrf_token)) {
            abort(403, 'Token mismatched!');
        } elseif (time() - $session_csrf_token_time >= self::getMaxTime()) {
            abort(403, 'Token expired!');
        }
    }

    /**
     * Get token duration.
     *
     * @return int
     */
    private static function getMaxTime()
    {
        return config('csrf_max_time') ?? self::$max_time;
    }
}
