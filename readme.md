# Boubou (PHP Framework)

A small PHP framework.

I write this framework to consolidate my PHP knowledge. That's why It does not use any external dependencies.
I work on the project during my free time, hence the little update. Feedback welcome at fpglaurent@gmail.com.

It is freely inspired by the fantastic work of Fabien Potencier with [Symfony](https://symfony.com) and in particular Taylor Otwell with [Laravel](https://laravel.com/).<br/>
Even if the code is entirely different (and obviously one or two hundred notches below), it is nevertheless, somehow, similar to the syntax used in eloquent, blade, ...

- Boubou is a MVC architectural pattern.
- It has an ORM who support some relationship: hasOne, belongsTo, hasMany and belongsToMany.
- It can eager load sub-models.
- It has a simple *routing system*.
- It support *middlewares* for your controllers (WIP).
- And propose some interesting stuff in its *template system*.

<span style="color:#dc3545">**Again, this framework is for learning purposes only. It may not be secure enough, and therefore not recommended for a project in production.**</span>

## A blog
This project contains all the necessary files for a basic blog.<br />
Some methods of account creation and authentication are also provided.
> Demo available at [getboubou.com](https://getboubou.com/).

## Requirments

- PHP 7+
- Sendmail (optional)

## Configuration
Open `config.php`.<br />
You will find the different configuration properties of your application:
- the duration of the csrf token,
- the mime types authorized for the file upload,
- the properties of the content pagination,
- and the session parameters.

## Database
Boubou support **PostgreSQL**, **MySQL** ans **SQLite**.<br/>
Edit the `.env` file to insert your own connection settings.
```
DB_CONNECTION=pgsql
DB_PORT=5432
DB_HOST=127.0.0.1
DB_DATABASE=bouboudb
DB_USERNAME=boubou
DB_PASSWORD=secret
```

### PostgreSQL
```bash
sudo -i -u postgres
```
Then create your database:
```sql
psql -U postgres -h localhost
CREATE USER boubou;
ALTER USER boubou WITH SUPERUSER PASSWORD 'secret';
CREATE DATABASE bouboudb;
ALTER DATABASE bouboudb OWNER TO boubou;
\q
```

### MySQL
Update your `.env` with:
```
DB_CONNECTION=mysql
DB_PORT=3306
```

And create your database:
```sql
sudo mysql;
CREATE USER 'boubou'@'localhost' PASSWORD EXPIRE NEVER;
ALTER USER 'boubou'@'localhost' IDENTIFIED BY 'secret';
CREATE DATABASE bouboudb;
GRANT ALL ON *.* TO 'boubou'@'localhost';
exit
```

### SQLite
```bash
sudo apt install php-sqlite3
```
Then restart your server (nginx, apache, ...)

Update your `.env` with:
```
DB_CONNECTION=sqlite
DB_DATABASE=./storage/sqlite/ypur-database-name.db
```

## Migration
The migration file will be created in `database/`.<br/>
It is recommended to name the tables in the plural.

```bash
php boubou migrate:create create_examples_table
```

This command clears your previous tables and restarts all migrations.
```bash
php boubou migrate
```

## Model
Create a new model (stored in `app/`).

```bash
php boubou model:create Example
```

It is recommended to name the model in the singular. Useful for the framework to guess the join keys.<br/>
For example, a `post_id` column that points to a `posts` table, the foreign key will be guessed on the name of the model `Post`.<br />

If the plural is too different from the singular, for example `category` and `categories`, know that you can force the name of your table in your model. For example with `protected $table = 'categories';` which avoids looking for an odd plural form like <del title="categorys">`categorys`</del>.

Take a look at the sample blog models to see how models are related to each other.


### Relation
You can define different relationships between your models:
- *hasOne* (one to one),
- *belongsTo* (many to one),
- *hasMany* (one to many),
- and *belongsToMany* (many to many).

```php
/**
 * Blog post model.
 */
class Post extends Model
{
    /**
     * Get the user record associated with the post.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The categories that belong to the post.
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
```

### Orm
You can use the Orm implemented in the models for your SQL queries.
```php
$post = Post::select('id', 'title', 'slug')
    ->where('slug', '=', $slug)
    ->orderBy('id', 'desc')
    ->first();
```
Is equivalent to:
```sql
SELECT id, title, slug FROM posts WHERE slug = :slug ORDER BY id desc LIMIT 1;
```
#### Eager loading
You can also eager load your model with your submodels, which avoids many unnecessary queries. Use the `with()` method :
```php
$posts = Post::with('user', 'categories', 'tags')
    ->orderBy('id', 'desc')
    ->get();
```

#### Pagination
The pagination is done by replacing the method `get()` by `paginate()`.<br />
```php
$posts = Post::with('user', 'categories', 'tags')
    ->orderBy('id', 'desc')
    ->paginate();
```
Then add page-by-page navigation in your views:
```mustache
<ul>
    <li>
        <a href="?page={{ $list->previousPage() }}{{ ! empty($q) ? '&q=' . urlencode($q) : '' }}">
            Previous
        </a>
    </li>
    @for ($i = $list->startPage(); $i <= $list->maxPage(); $i++)
        @if ($i <= $list->numberOfPages())
            <li>
                <a href="?page={{ $i }}{{ ! empty($q) ? '&q=' . urlencode($q) : '' }}">{{ $i }}</a>
            </li>
        @else
            <li><a>{{ $i }}</a></li>
        @endif
    @endfor
    <li>
        <a href="?page={{ $list->nextPage() }}{{ ! empty($q) ? '&q=' . urlencode($q) : '' }}">
            Next
        </a>
    </li>
</ul>
```
See `pagination` in `config.php` to define the default values.<br />
You can override the `per_page` in the method, for example for 30 results per page: `paginate(30)`.


## Controller
The created controller will be filled with the basic methods of a **CRUD** (`app/Controllers/`).
```bash
php boubou controller:create ExamplesController
```

## Middleware
The framework supports middleware (work in progress).<br />
The middlewares are stored in `app/Middleware/`.

For now, only the middlewares directly invoked in the controllers are implemented.
```php
class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(AuthMiddleware::class, ['redirect' => '/login', 'except' => ['index', 'show']]);
    }
}
```

## View (template)
The views are inspired by the blade syntax (if you already know **{{ mustache }}** the syntax will seem familiar). They are located in `resources/views/`.<br/>
The files are written to the cache directory in `storage/cache/`. This directory must have write permissions, if necessary do a `sudo chmod -R 755 storage/cache/`.

## Route
A single file to contain your routes (`routes.php`).<br />
The authorized verbs are: *delete*, *get*, *patch*, *post*, *put*.
```php
Route::get('/your/url1', 'NameOfTheController@NameOfTheMethod');
Route::post('/your/url2', 'NameOfTheController@AnotherMethod');
```
or, for something more readable:
```php
Route::prefix('/your', function () {
    Route::get('/url1', 'NameOfTheController@NameOfTheMethod');
    Route::post('/url2', 'NameOfTheController@AnotherMethod');
});
```

## File
Your uploaded files are put into the storage directory. To make them accesible from your page, you have to make a sym link to your public directory. Where *$PATH_TO_PROJECT* is yout path, ie.: `/home/MyUser/src/boubou`
```bash
ln -s $PATH_TO_PROJECT/storage/uploads $PATH_TO_PROJECT/public/
```
### Upload/remove a file
To upload a file, add this input to your view:
```html
<input type="file" name="myfile" />
```

Add this line to your controller:
```php
use Boubou\Core\File;
```

Then to upload a file:
Note that the "file" request will be converted on the fly before arriving to your method.
```php
public function uploadMyFile(Request $request)
{
    $prefix = 'post-xxx';
    $myfile = $request->myfile;
    $filename = $prefix . '.' . $myfile->extension();
    if ($myfile->moveToStorage($filename)) {
        // Uploaded!
    } else {
        // Not uploaded!
    }
}
```

To remove a file:
```php
$thefile = 'filetoremove.jpg';
File::unlinkFromStorage($thefile);
```

## Secure

### Display errors on screen.
Change `APP_ENV` to `production` to not print php error. Set it to `local` for debug.
```
APP_ENV=local
```

### XSS (Cross-site Scripting)
Variables displayed with `{{ $var }}` are protected with htmlspecialchars.<br />
If you do not want to protect them use `{!! $var !!}`, for example to use a `{!! nl2br($var) !!}`.<br />
Note that in any cases, the **php**, **script** and **style** tags contained in `$var` will be protected.

### CSRF (Cross-site request forgery)
All your forms with the "post" method must contain the following field.
```html
<input type="hidden" name="csrf_token" value="{{ Csrf::token() }}" />
```

### Obfuscate a string
You can obfuscate a string in the rendered view. For example an email address, with `{!! obfuscate('toto@example.com') !!}`.

```html
&#116;&#x6f;t&#x6f;@ex&#x61;&#109;pl&#101;&#46;c&#111;&#x6d;
```

### Upload
The upload directory `storage/uploads/` is protected by its own `.htaccess`. It forbids the execution of PHP.<br />
You can change the ***mime types*** authorized to upload in `config.php`.

<!-- ### Denial of Service (DoS)
TODO: DOS attack -->

## Other
Use this for more commands.
```bash
php boubou help
```

## Mail
You can send your mails with sendmail. Leave the `MAIL_DRIVER` variable empty (`.env`).

Mail not sent? Look at your `storage/logs/boubou.log` to check is there is an error.<br />
With Gmail, dont forget to enable [Less secure apps](https://support.google.com/accounts/answer/6010255?hl=en).<br />

Mail in spam? Dont forget to modify your DNS with SPF (Sender Policy Framework), DKIM (DomainKeys Identified Mail) and DMARC (Domain-based Message Authentication, Reporting and Conformance). Follow this link for [Google](https://support.google.com/a/answer/33786?hl=en).

### SMTP
You can send your mails by SMTP. Fill in the `MAIL_DRIVER` variable to "smtp".<br />
For example to send your mails with Gmail:
```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=465
MAIL_USERNAME=example@gmail.com
MAIL_PASSWORD=xxxxxx
MAIL_ENCRYPTION=tls
```

### Sending mails
Add this line to your controller:
```php
use Boubou\Core\Email;
```

Then you can send your mails with different chained options. For example:
```php
if (Email::isValid($user->email)) {
    $sent = Email::to($user->email)
        ->cc('contact@example.com', 'Copy to Contact')
        ->bcc('admin@example.com', 'Blind carbon copy to Admin')
        ->subject('Wesh!')
        ->message('A mail from <b>getboubou.com</b>!')
        ->send();
    if ($sent) {
        // Mail sent!
    } else {
        // Mail not sent!
        $errors = Email::getErrors();
        if (count($errors)) {
            dd($errors);
        }
    }
}
```
Note: "from" is filled by default with MAIL_USERNAME and APP_NAME.

## Run (local)
```bash
cd boubou
php -S localhost:8000 index.php
```

## Debug

Be careful, the following methods have effect even if `APP_ENV` is set to "production".

### Variable debug
Use the `dump()` or `dd()` helper. "dd" stands for "dump & die".
```php
$variable1 = 101;
dump($variable1, 100, 99, -98.0, 12.343, 13.000, 'toto', '<span style="color:#1299DA">SPAN!</span>', " ", date('l \t\h\e jS'), 10 > 8, ['a', 'b', -4.01], (object) ['c' => new \stdClass, 'd' => 10e34]);
```
<details>
<summary>It will produce a code like this:</summary>
<pre style="background-color:white;color: black;border: 1px solid #ccc; padding: 10px 10px 12px 12px; display: inline-block; clear: both; font: 11px monospace; line-height: 1.5em; word-wrap: break-word; word-break: break-all;"><p><small style="color:#666">/app/Controllers/BlogController.php:59</small></p>
<span style="color:#1299DA">int</span>(<span style="color:#FF8400">101</span>) <span style="color:#32CD32">101</span>
<span style="color:#1299DA">int</span>(<span style="color:#FF8400">100</span>) <span style="color:#32CD32">100</span>
<span style="color:#1299DA">int</span>(<span style="color:#FF8400">99</span>) <span style="color:#32CD32">99</span>
<span style="color:#1299DA">float</span>(<span style="color:#FF8400">-98</span>) <span style="color:#32CD32">-98</span>
<span style="color:#1299DA">float</span>(<span style="color:#FF8400">12.343</span>) <span style="color:#32CD32">12.343</span>
<span style="color:#1299DA">float</span>(<span style="color:#FF8400">13</span>) <span style="color:#32CD32">13</span>
<span style="color:#1299DA">string</span>(<span style="color:#FF8400">4</span>) "<span style="color:#666666">toto</span>"
<span style="color:#1299DA">string</span>(<span style="color:#FF8400">40</span>) "<span style="color:#666666">&lt;span style="color:#1299DA"&gt;SPAN!&lt;/span&gt;</span>"
<span style="color:#1299DA">string</span>(<span style="color:#FF8400">1</span>) "<span style="color:#666666"> </span>"
<span style="color:#1299DA">string</span>(<span style="color:#FF8400">17</span>) "<span style="color:#666666">Saturday the 20th</span>"
<span style="color:#1299DA">bool</span>(<span style="color:#FF8400">true</span>) <span style="color:#32CD32">true</span>
<span style="color:#1299DA">array</span>(<span style="color:#FF8400">3</span>) [
    [<span style="color:#FF8400">0</span>] = <span style="color:#1299DA">string</span>(<span style="color:#FF8400">1</span>) "<span style="color:#666666">a</span>"
    [<span style="color:#FF8400">1</span>] = <span style="color:#1299DA">string</span>(<span style="color:#FF8400">1</span>) "<span style="color:#666666">b</span>"
    [<span style="color:#FF8400">2</span>] = <span style="color:#1299DA">float</span>(<span style="color:#FF8400">-4.01</span>) <span style="color:#32CD32">-4.01</span>
]
<b style="color:#1299DA">class</b> <b style="color:#c400ff">stdClass</b>(<span style="color:#FF8400">2</span>) {
    [c] = <b style="color:#1299DA">class</b> <b style="color:#c400ff">stdClass</b>(<span style="color:#FF8400">0</span>) {}
    [d] = <span style="color:#1299DA">float</span>(<span style="color:#FF8400">1.0E+35</span>) <span style="color:#32CD32">1.0E+35</span>
}
</pre>
</details>

### Query debug
To display the query prepared with the Orm before execution.
Just replace the last method first() or get() with **toSql()**.
```php
$debug = Post::select('id', 'title', 'slug')
    ->where('slug', '=', $slug)
    ->orderBy('id', 'desc')
    ->toSql();
dd($debug);
```
<details>
<summary>It will produce a code like this:</summary>
<pre style="background-color:white;color: black;border: 1px solid #ccc; padding: 10px 10px 12px 12px; display: inline-block; clear: both; font: 11px monospace; line-height: 1.5em; word-wrap: break-word; word-break: break-all;"><p><small style="color:#666">/app/Controllers/BlogController.php:62</small></p>
<span style="color:#1299DA">string</span>(<span style="color:#FF8400">69</span>) "<span style="color:#666666">SELECT id, title, slug FROM posts WHERE slug = :slug ORDER BY id desc</span>"
</pre>
</details>

### Log messages
A `boubou.log` file contains the logs that you want to keep, it is stored in `storage/logs/boubou.log`.<br />
To write log in it, add this line at the top of your PHP file.
```php
use Boubou\Core\Log;
```
Then call one of the following methods: *error*, *warning*, *notice*, *info* or *debug*.<br />
Where `(array) $context` (optional) are additional data to describe the message. Ie: `['toto', 123, $variable]`.
```php
Log::error($message, $context);
```
It appends a new line like this:
```log
[2018-12-24 18:25:00][81.241.228.178] local.ERROR Oups! There is an error! ["tata",456]
```

## Security Vulnerabilities
If you discover a security vulnerability, please send an e-mail to [fpglaurent@gmail.com](mailto:fpglaurent@gmail.com).

## License
The Boubou framework is open-sourced software licensed under the MIT license.
