<?php

namespace App;

use Boubou\Core\Model;

/**
 * Blog post model.
 */
class Post extends Model
{
    /**
     * @var array The fillable columns (fields).
     */
    protected $fillable = [
        'title',
        'slug',
        'picture',
        'resume',
        'article',
        // 'user_id', // Useless when model key is defined by the relation. Ie.: $user->posts().
        'published_at',
    ];

    /**
     * Get the user record associated with the post.
     *
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The categories that belong to the post.
     *
     * @return App\Category
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * The tags that belong to the post.
     *
     * @return App\Tag
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
