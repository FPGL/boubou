<?php

namespace App\Controllers;

use App\Category;
use App\Middleware\Auth as AuthMiddleware;
use Boubou\Core\Controller;
use Boubou\Core\Request;

/**
 * Categories controller.
 */
class CategoriesController extends Controller
{
    /**
     * Declare some middleware.
     */
    public function __construct()
    {
        $this->middleware(AuthMiddleware::class, ['redirect' => '/login']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::orderBy('id', 'desc')->paginate();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = $data['slug'] ?: $data['name'];
        $data['slug'] = slug($data['slug']);

        $category = Category::create($data);

        return redirect('/admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $category = Category::where('id', '=', $id)->firstOrFail();

        return view('admin.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $category = Category::where('id', '=', $id)->firstOrFail();

        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['slug'] = $data['slug'] ?: $data['name'];
        $data['slug'] = slug($data['slug']);

        $category = Category::find($id);
        $category->update($data);

        return redirect('/admin/categories/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        Category::where('id', '=', $id)->delete();

        return redirect('/admin/categories');
    }
}
