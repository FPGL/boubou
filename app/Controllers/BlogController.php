<?php

namespace App\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use App\User;
use Boubou\Core\Controller;
use Boubou\Core\Request;

/**
 * Blog controller.
 */
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $q = $request->q ?: null;
        if ($q) {
            // Ugly search (ILIKE).
            $posts = Post::with('user', 'categories', 'tags')
                ->where('title', 'ILIKE', "%$q%")
                ->orderBy('id', 'desc')
                ->paginate();
        } else {
            $posts = Post::with('user', 'categories', 'tags')
                ->orderBy('id', 'desc')
                ->paginate();
        }

        $categories = Category::orderBy('name', 'desc')->get();
        $tags = Tag::orderBy('name', 'desc')->get();

        return view('blog.index', compact('posts', 'categories', 'tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request, $slug)
    {
        $post = Post::with('user', 'categories', 'tags')
            ->where('slug', '=', $slug)
            ->orderBy('id', 'desc')
            ->firstOrFail(); // 404 if no "post" found
        // ->first(); // 500 because the blog.show can not render with an empty "post".

        $categories = Category::orderBy('name', 'desc')->get();
        $tags = Tag::orderBy('name', 'desc')->get();

        return view('blog.show', compact('post', 'categories', 'tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function user(Request $request, $id)
    {
        $user = User::where('id', '=', $id)->firstOrFail();

        $categories = Category::orderBy('name', 'desc')->get();
        $tags = Tag::orderBy('name', 'desc')->get();

        return view('blog.user', compact('user', 'categories', 'tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function category(Request $request, $slug)
    {
        $category = Category::where('slug', '=', $slug)->firstOrFail();

        $categories = Category::orderBy('name', 'desc')->get();
        $tags = Tag::orderBy('name', 'desc')->get();

        return view('blog.category', compact('category', 'categories', 'tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function tag(Request $request, $slug)
    {
        $tag = Tag::where('slug', '=', $slug)->firstOrFail();

        $categories = Category::orderBy('name', 'desc')->get();
        $tags = Tag::orderBy('name', 'desc')->get();

        return view('blog.tag', compact('tag', 'categories', 'tags'));
    }
}
