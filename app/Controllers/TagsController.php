<?php

namespace App\Controllers;

use App\Tag;
use App\Middleware\Auth as AuthMiddleware;
use Boubou\Core\Controller;
use Boubou\Core\Request;

/**
 * Categories controller.
 */
class TagsController extends Controller
{
    /**
     * Declare some middleware.
     */
    public function __construct()
    {
        $this->middleware(AuthMiddleware::class, ['redirect' => '/login']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tags = Tag::orderBy('id', 'desc')->paginate();

        return view('admin.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = $data['slug'] ?: $data['name'];
        $data['slug'] = slug($data['slug']);

        $tag = Tag::create($data);

        return redirect('/admin/tags');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $tag = Tag::where('id', '=', $id)->firstOrFail();

        return view('admin.tags.show', compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $tag = Tag::where('id', '=', $id)->firstOrFail();

        return view('admin.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['slug'] = $data['slug'] ?: $data['name'];
        $data['slug'] = slug($data['slug']);

        $tag = Tag::find($id);
        $tag->update($data);

        return redirect('/admin/tags/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        Tag::where('id', '=', $id)->delete();

        return redirect('/admin/tags');
    }
}
