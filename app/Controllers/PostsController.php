<?php

namespace App\Controllers;

use App\Category;
use App\Middleware\Auth as AuthMiddleware;
use App\Post;
use App\Tag;
use Boubou\Core\Auth;
use Boubou\Core\Controller;
use Boubou\Core\File;
use Boubou\Core\Request;

/**
 * Blog posts controller.
 */
class PostsController extends Controller
{
    /**
     * Declare some middleware.
     */
    public function __construct()
    {
        $this->middleware(AuthMiddleware::class, ['redirect' => '/login']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $q = $request->q ?: null;
        if ($q) {
            // Ugly search.
            $posts = Post::orderBy('id', 'desc')->where('title', 'ILIKE', "%$q%")->paginate();
        } else {
            $posts = Post::orderBy('id', 'desc')->paginate();
        }

        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::get();
        $tags = Tag::get();

        return view('admin.posts.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $data = $request->all();
        $data['slug'] = $data['slug'] ?: $data['title'];
        $data['slug'] = slug($data['slug']);

        $post = $user->posts()->create($data);
        $post->categories()->sync($data['category_ids']);
        if (isset($data['tag_ids'])) {
            $post->tags()->sync($data['tag_ids']);
        }

        $this->uploadFile($post, $request->file);

        return redirect('/admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $post = Post::where('id', '=', $id)->firstOrFail();

        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $post = Post::where('id', '=', $id)->firstOrFail();
        $categories = Category::get();
        $tags = Tag::get();

        return view('admin.posts.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['slug'] = $data['slug'] ?: $data['title'];
        $data['slug'] = slug($data['slug']);

        $post = Post::find($id);
        $post->update($data);
        $post->categories()->sync($data['category_ids']);
        if (isset($data['tag_ids'])) {
            $post->tags()->sync($data['tag_ids']);
        }
        $this->uploadFile($post, $request->file);

        return redirect('/admin/posts/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        Post::where('id', '=', $id)->delete();

        return redirect('/admin/posts');
    }

    /**
     * Move uploaded file.
     *
     * @param Post $post
     * @param File $file
     *
     * @return void
     */
    public function uploadFile($post, $file)
    {
        if ($file->size) {
            if ($post->picture) {
                File::unlinkFromStorage($post->picture);
            }
            $new_name = 'post-' . $post->id . '.' . $file->extension();
            if ($file->moveToStorage($new_name)) {
                $post->picture = $new_name;
                $post->save();
            }
        }
    }
}
