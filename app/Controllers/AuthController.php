<?php

namespace App\Controllers;

use App\User;
use Boubou\Core\Auth;
use Boubou\Core\Controller;
use Boubou\Core\CustomException;
use Boubou\Core\Email;
use Boubou\Core\Request;

/**
 * Authentication controller.
 */
class AuthController extends Controller
{
    /**
     * @var string Where to redirect to the user when authenticated.
     */
    public $redirect_to = '/home';

    /**
     * Show login form.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        if (Auth::user()) {
            return redirect($this->redirect_to);
        }

        return view('auth.login');
    }

    /**
     * Show register form.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function register(Request $request)
    {
        if (Auth::user()) {
            return redirect($this->redirect_to);
        }

        return view('auth.register');
    }

    /**
     * Attempt to log the user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function attemptLogin(Request $request)
    {
        $user = User::select('id', 'password')
            ->where('email', '=', $request->email)
            ->first();
        if ($user) {
            $verified = password_verify($request->password, $user->password);
            if ($verified) {
                $request->session('user_id', $user->id);

                return redirect($this->redirect_to);
            }
        }

        return back();
    }

    /**
     * Store new user.
     *
     * @param Request $request
     *
     * @return Response Redirect to $this->redirect_to
     *
     * @throws CustomException If cannot create the new user.
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['password'] = \bcrypt($data['password']);
        $user = User::create($data);

        if ($user) {
            // Welcome mail
            if (Email::isValid($user->email)) {
                $sent = Email::to($user->email, $user->name)
                    ->bcc(env('MAIL_USERNAME'))
                    ->subject('Boubou!')
                    ->message('Wesh ' . $user->name . '!<br />
                        Thank you for signing up at getboubou.com.<br /><br />
                        <small>Boubou is a PHP microframework. If you do not solicit this email, please accept our apologies.</small>')
                    ->send();
            }

            // Immediately connect the created user.
            $this->attemptLogin($request);

            return redirect($this->redirect_to);
        } else {
            throw new CustomException('Cannot create new user!');
        }
    }

    /**
     * Logout.
     *
     * @return Response
     */
    public function logout()
    {
        session_destroy();

        return redirect('/');
    }
}
