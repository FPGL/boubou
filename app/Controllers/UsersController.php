<?php

namespace App\Controllers;

use App\Middleware\Auth as AuthMiddleware;
use App\Middleware\Role as RoleMiddleware;
use App\Role;
use App\User;
use Boubou\Core\Auth;
use Boubou\Core\Controller;
use Boubou\Core\Request;

/**
 * User controller.
 */
class UsersController extends Controller
{
    /**
     * Declare some middleware.
     */
    public function __construct()
    {
        // User must be logged to access this controller.
        $this->middleware(AuthMiddleware::class, ['redirect' => '/login']);
        // $this->middleware(AuthMiddleware::class, ['redirect' => '/login', 'except' => ['index', 'show']]);

        // User must have role "Admin" to access this controller. Except for the dashboard() method.
        $this->middleware(RoleMiddleware::class, ['roles' => ['Admin'], 'except' => ['dashboard'], 'message' => 'Permission denied!']);
    }

    /**
     * Display users list (admin).
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $users = User::select('id', 'name', 'password', 'email')
            ->orderBy('name', 'asc')
            ->orderBy('id', 'asc')
            ->paginate();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $roles = Role::get();

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['password'] = \bcrypt($data['password']);
        $user = User::create($data);
        $user->roles()->sync($data['role_ids']);

        return redirect('/admin/users/' . $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $user = User::where('id', '=', $id)->firstOrFail();

        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the edit form.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $user = User::where('id', '=', $id)->firstOrFail();
        $roles = Role::get();

        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update an existing resource.
     *
     * @param Request $request
     * @param int $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (empty($data['password'])) {
            // Empty password field is not applied to "users" table.
            unset($data['password']);
        } else {
            $data['password'] = bcrypt($data['password']);
        }

        $user = User::find($id);
        $user->update($data);
        $user->roles()->sync($data['role_ids']);

        if ($user->infos && is_int($user->infos->id)) {
            $user->infos->update($data);
        } else {
            $user->infos()->create($data);
        }

        return redirect('/admin/users/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {
        User::where('id', '=', $id)->delete();

        return redirect('/admin/users');
    }

    /**
     * UsersController
     */
    public function home(Request $request)
    {
        $user = Auth::user();

        return view('home', compact('user'));
    }

    /**
     * To home()
     */
    public function dashboard(Request $request)
    {
        return $this->home($request);
    }
}
