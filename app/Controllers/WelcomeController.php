<?php

namespace App\Controllers;

use App\Category;
use App\Post;
use Boubou\Core\Controller;
use Boubou\Core\Request;

/**
 * Welcome controller.
 */
class WelcomeController extends Controller
{
    /**
     * Display the welcome page.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function welcome(Request $request)
    {
        return view('welcome');
    }
}
