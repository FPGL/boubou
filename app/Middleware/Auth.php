<?php

namespace App\Middleware;

use Boubou\Core\Request;
use Closure;

/**
 * Authentication middleware.
 */
class Auth
{
    /**
     * Run the request filter.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $args)
    {
        // Allowed methods without any user "role".
        if (! isset($args['except'])) {
            $args['except'] = [];
        }

        if (! in_array($request->method, $args['except'])) {
            $connected = $request->session('user_id');
            if (! $connected) {
                if ($args['redirect']) {
                    redirect($args['redirect']);
                } else {
                    abort(403, 'Access denied!');
                }
            }
        }

        return $next($request);
    }
}
