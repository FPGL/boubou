<?php

namespace App\Middleware;

use Boubou\Core\Request;
use Closure;

/**
 * User's role middleware.
 */
class Role
{
    /**
     * Run the request filter.
     *
     * @param Request $request
     * @param Closure $next
     * @param array $role
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $args)
    {
        // Allowed methods without any user "role".
        if (! isset($args['except'])) {
            $args['except'] = [];
        }

        // Error message if no role matches.
        if (! isset($args['message'])) {
            $args['message'] = 'Access denied!';
        }

        $found = false;
        $user = $request->user();

        if ($user && in_array($request->method, $args['except'])) {
            $found = true;
        }

        if ($user) {
            foreach ($args['roles'] as $role) {
                if ($user->hasRole($role)) {
                    $found = true;
                    break;
                }
            }
        }

        if (! $found) {
            return abort(403, $args['message']);
        }

        return $next($request);
    }
}
