<?php

namespace App;

use Boubou\Core\Model;

/**
 * Tag model.
 */
class Tag extends Model
{
    /**
     * @var array The fillable columns (fields).
     */
    protected $fillable = [
        'name', 'slug', 'description'
    ];

    /**
     * The posts that belong to the tag.
     *
     * @return App\Post
     */
    public function posts()
    {
        // return $this->belongsToMany(Post::class, 'tags_posts', 'post_id', 'tag_id');
        return $this->belongsToMany(Post::class, 'tags_posts');
    }
}
