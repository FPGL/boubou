<?php

namespace App;

use Boubou\Core\Model;

/**
 * User roles model.
 */
class Role extends Model
{
    /**
     * @var array The fillable columns (fields).
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * The users that belong to the role.
     *
     * @return App\User
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
