<?php

namespace App;

use App\User;
use Boubou\Core\Model;

/**
 * User infos model.
 */
class UserInfo extends Model
{
    /**
     * @var string The table associated with the model.
     */
    protected $table = 'user_infos';

    /**
     * @var array The fillable columns (fields).
     */
    protected $fillable = [
        'user_id',
        'firstname',
        'lastname',
    ];

    /**
     * Retrieves the user record associated with user info.
     *
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
