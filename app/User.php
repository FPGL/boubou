<?php

namespace App;

use Boubou\Core\Model;

/**
 * User model.
 */
class User extends Model
{
    /**
     * @var array The fillable columns (fields).
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * Check the user role.
     *
     * @param string $role Verify the role expected
     *
     * @return bool
     */
    public function hasRole($role)
    {
        return $this->roles()->has($role, 'name');
    }

    /**
     * Get the infos record associated with the user.
     *
     * @return App\UserInfo
     */
    public function infos()
    {
        return $this->hasOne(UserInfo::class);
    }

    /**
     * Get the posts associated with the user.
     *
     * @return App\Post
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * The roles that belong to the user.
     *
     * @return App\Role
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'roles_users');
    }
}
