<?php

namespace App;

use Boubou\Core\Model;

/**
 * Category model.
 */
class Category extends Model
{
    /**
     * @var string The table associated with the model.
     */
    protected $table = 'categories';

    /**
     * @var array The fillable columns (fields).
     */
    protected $fillable = [
        'name', 'slug', 'description'
    ];

    /**
     * The posts that belong to the category.
     *
     * @return App\Post
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class, 'categories_posts');
    }
}
