<?php

/**
 * Application config parameters.
 */
return [

    /**
     * @var int Default token duration (second).
     */
    'csrf_max_time' => 180,

    /**
     * @var array Allowed mime types to upload.
     */
    'allowed_mimes' => [
        'jpg|jpeg|jpe' => 'image/jpeg',
        'gif' => 'image/gif',
        'png' => 'image/png',
    ],

    /**
     * @var array Pagination.
     */
    'pagination' => [
        'bullets_max' => 5,
        'per_page' => 5,
    ],

    /*
    |--------------------------------------------------------------------------
    | Session
    |--------------------------------------------------------------------------
    */

    /**
     * @var int Session lifetime
     */
    'session' => [

        // Cookie name
        'name' => 'boubou_session',

        // Sessions parameters
        'lifetime' => 86400 * 7, // 86400 seconds = 24 hours,
        'path' => '/',
        'domain' => null,
        'secure' => false,
        'httponly' => false,
    ],

];
